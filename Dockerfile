FROM node:20-alpine as build

WORKDIR /app

COPY package*.json ./

RUN npm ci --silent

COPY . ./

EXPOSE 5200

CMD ["node", "app.js"]

