import Logger from "../config/logger.js";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import { getConnection } from "../config/db.js";
import dotenv from "dotenv";
import sendInvitationEmail from "../functions/emailInvitation.js";
import { generatePasswordResetTicket, generateRandomPassword, getAccessToken } from "../functions/auth0.js";
import { BlobServiceClient, StorageSharedKeyCredential} from '@azure/storage-blob';
import { uploadFileToBlob,getBlobUrl,deleteFileFromBlob } from "../functions/azureBlobStorage.js";
import 'dotenv/config';
import axios from "axios";

export const getAllUsers = async (req, res) => {
  try {
    const pool = await getConnection();
    const result = await pool.request().query(`
      SELECT * FROM [dbo].[User]
    `);

    const emp = result.recordset;

    if (emp && emp.length > 0) {
      Logger.info("The User details were fetched successfully", {
        service: "user_details",
      });
      res.status(200).json({
        success: true,
        message: "The User details were fetched successfully",
        data: emp,
      });
    } else {
      Logger.info("No User details were found", { service: "user_details" });
      res.status(404).json({
        success: false,
        message: "No User details were found",
      });
    }
  } catch (error) {
    console.error(error);
    Logger.error("User fetch failed", { service: "user_details", error });
    res.status(500).json({
      success: false,
      message: "User fetch failed",
      error: error.message,
    });
  }
};

export const getUserById = async (req, res) => {
  try {
    const pool = await getConnection();
    const result = await pool.request().input("UserID", req.params.id).query(`
        SELECT 
          [UserID], [UserFirstName], [UserLastName], [UserEmail], [UserPhoneNumber], [UserAge], 
          [UserGender], [UserEducationLevel], [UserOccupation], [UserIncomeRange], [UserCountry], 
          [UserState], [UserCity], [UserLinkedInProfileLink], [UserFacebookProfileLink], 
          [UserInstagramProfileLink], [UserTwitterProfileLink], [UserCurrentCompanyLink], 
          [UserPastCompaniesLink], [UserCompanyID], [AuthId], [UserCommunicationEmail], 
          [UserPhone], [UserStreet1], [UserStreet2], [UserZip], [UserJobTitle], [UserCompanyEmail], 
          [UserCompanyName], [UserJobIndustry], [UserProfessionalExpertise], [UserProfessionalSkills], 
          [UserCommunityInvolvement], [UserUniversityAttended], [UserGraduationYear], 
          [UserLeadershipProgramGraduatedFrom], [UserIsToBeRegistered], [UserIsAlumni], 
          [UserIsInCurrentClass], [UserIsChamberMember], [UserLeadershipBatchYear], 
          [UserInterestedInLeadershipTopics], [UserSeekingMentor], [UserWillingToBeMentor], 
          [UserMentorshipInvolvementList], [UserWillingToBeGuestSpeaker], [UserGuestSpeakerTopicPreference], 
          [UserIsActive], [UserMemberSinceDate], [UserLastUpdated], [UserIsAdmin]
        FROM [dbo].[User]
        WHERE [UserID] = @UserID
      `);

    const emp = result;

    if (emp && emp.length > 0) {
      Logger.info("The User details were fetched successfully", {
        service: "user_details",
      });
      res.status(200).json({
        success: true,
        message: "The User details were fetched successfully",
        data: emp,
      });
    } else {
      Logger.info("No User details were found", { service: "user_details" });
      res.status(404).json({
        success: false,
        message: "No User details were found",
      });
    }
  } catch (error) {
    console.error(error);
    Logger.error("User fetch failed", { service: "user_details", error });
    res.status(500).json({
      success: false,
      message: "User fetch failed",
      error: error.message,
    });
  }
};

dotenv.config();

// controllers/user.js

export const registerUser = async (req, res) => {
  const { UserFirstName, UserLastName, UserEmail } = req.body;

  try {
    const pool = await getConnection();

    // Check if the user already exists
    const userCheckResult = await pool
      .request()
      .input("UserEmail", sql.NVarChar, UserEmail)
      .query(`SELECT * FROM [dbo].[User] WHERE UserEmail = '@UserEmail'`);

    const userCheck = userCheckResult.recordset;

    if (userCheck && userCheck.length > 0) {
      return res.status(400).json({
        success: false,
        message: "User already exists",
      });
    }

    // Hash the password
    //   const salt = await bcrypt.genSalt(10);
    //   const hashedPassword = await bcrypt.hash(UserPassword, salt);
    const UserPassword = "12345";
    // Insert new user
    await // .input('UserPassword', sql.NVarChar, hashedPassword)
    pool
      .request()
      .input("UserFirstName", sql.NVarChar, UserFirstName)
      .input("UserLastName", sql.NVarChar, UserLastName)
      .input("UserEmail", sql.NVarChar, UserEmail).query(`
          INSERT INTO [dbo].[User] (UserFirstName, UserLastName, UserEmail, UserPassword) 
          VALUES (@UserFirstName, @UserLastName, @UserEmail, @UserPassword)
        `);

    Logger.info("User registered successfully", { service: "user_details" });
    res.status(201).json({
      success: true,
      message: "User registered successfully",
    });
  } catch (error) {
    console.error(error);
    Logger.error("User registration failed", {
      service: "user_details",
      error,
    });
    res.status(500).json({
      success: false,
      message: "User registration failed",
      error: error.message,
    });
  }
};

export const loginUser = async (req, res) => {
  const { UserEmail, UserPassword } = req.body;

  try {
    const pool = await getConnection();

    // Check if the user exists
    const userResult = await pool
      .request()
      .input("UserEmail", sql.NVarChar, UserEmail)
      .query("SELECT * FROM [dbo].[User] WHERE UserEmail = @UserEmail");

    const user = userResult.recordset;

    if (user && user.length === 0) {
      return res.status(400).json({
        success: false,
        message: "Invalid email or password",
      });
    }

    const foundUser = user[0];

    // Validate password
    const isMatch = await bcrypt.compare(UserPassword, foundUser.UserPassword);
    if (!isMatch) {
      return res.status(400).json({
        success: false,
        message: "Invalid email or password",
      });
    }

    // Generate JWT
    const payload = { userId: foundUser.UserID };
    const token = jwt.sign(payload, process.env.JWT_SECRET, {
      expiresIn: "1h",
    });

    Logger.info("User logged in successfully", { service: "user_details" });
    res.status(200).json({
      success: true,
      message: "User logged in successfully",
      token,
    });
  } catch (error) {
    console.error(error);
    Logger.error("User login failed", { service: "user_details", error });
    res.status(500).json({
      success: false,
      message: "User login failed",
      error: error.message,
    });
  }
};

export const getRoles = async (req, res) => {
  try {
    const pool = await getConnection();
    const result = await pool.request().query("SELECT * FROM [dbo].[Roles]");

    if (result.recordset.length > 0) {
      Logger.info("User roles fetched successfully", {
        service: "user_details",
      });
      res.status(200).json({
        success: true,
        message: "User roles fetched successfully",
        data: result.recordset,
      });
    } else {
      Logger.info("No roles found", { service: "user_details" });
      res.status(404).json({
        success: false,
        message: "No roles found",
      });
    }
  } catch (error) {
    Logger.error("Error fetching user roles", {
      service: "user_details",
      error,
    });
    res.status(500).json({
      success: false,
      message: "Internal server error",
      error: error.message,
    });
  }
};

export const getUserApproval = async (req, res) => {
  try {
    const pool = await getConnection();
    const result = await pool.request().query(`
      SELECT 
      u.[UserID],
      CONCAT(u.[UserFirstName], ' ', u.[UserLastName]) AS name,
      u.[UserEmail],
      CAST(a.[IsApproved] AS INT) as IsApproved,
      a.[ApprovalType],
      a.[RoleID],
      a.[SurveyProductID],
      a.[SurveyID],
      a.[ConsultantID]
    FROM [dbo].[User] u
    JOIN [dbo].[Approval] a ON u.[UserID] = a.[UserID]
    ORDER BY u.[UserID];`);

    Logger.info("User approvals fetched successfully", {
      service: "user_details",
    });
    res.status(200).json({
      success: true,
      message: "User approvals fetched successfully",
      data: result.recordset, // only sending the relevant recordset
    });
  } catch (error) {
    Logger.error("Error fetching user approvals", {
      service: "user_details",
      error,
    });
    res.status(500).json({
      success: false,
      message: "Error fetching user approvals",
      error: error.message,
    });
  }
};

export const getIndividualUsers = async (req, res) => {
  try {
    const pool = await getConnection();
    const result = await pool.request().query(`
      SELECT 
      u.[UserID],
      CONCAT(u.[UserFirstName], ' ', u.[UserLastName]) AS name,
      u.[UserEmail],
      CAST(a.[IsApproved] AS INT) as IsApproved,
      a.[ApprovalType],
      a.[RoleID],
      a.[SurveyProductID],
      a.[SurveyID],
      a.[ConsultantID]
    FROM [dbo].[User] u
    JOIN [dbo].[Approval] a ON u.[UserID] = a.[UserID]
    WHERE a.[ApprovalType] = 'User Signup Approval' AND (u.[UserCompanyName] IS NULL OR u.[UserCompanyName] = '')
    ORDER BY u.[UserID] DESC;`);

    Logger.info("Individual users fetched successfully", {
      service: "user_details",
    });
    res.status(200).json({
      success: true,
      message: "Individual users fetched successfully",
      data: result.recordset, 
    });
  } catch (error) {
    Logger.error("Error fetching individual users", {
      service: "user_details",
      error,
    });
    res.status(500).json({
      success: false,
      message: "Error fetching individual users",
      error: error.message,
    });
  }
};


export const getUsersWithCompany = async (req, res) => {
  try {
    const pool = await getConnection();
    const result = await pool.request().query(`
      SELECT 
      u.[UserID],
      CONCAT(u.[UserFirstName], ' ', u.[UserLastName]) AS name,
      u.[UserEmail],
      CAST(a.[IsApproved] AS INT) as IsApproved,
      a.[ApprovalType],
      a.[RoleID],
      a.[SurveyProductID],
      a.[SurveyID],
      a.[ConsultantID],
      u.[UserCompanyName]
    FROM [dbo].[User] u
    JOIN [dbo].[Approval] a ON u.[UserID] = a.[UserID]
    WHERE a.[ApprovalType] = 'User Signup Approval' AND (u.[UserCompanyName] IS NOT NULL AND u.[UserCompanyName] <> '')
    ORDER BY u.[UserID] DESC;`);

    Logger.info("Users with company fetched successfully", {
      service: "user_details",
    });
    res.status(200).json({
      success: true,
      message: "Users with company fetched successfully",
      data: result.recordset, 
    });
  } catch (error) {
    Logger.error("Error fetching users with company", {
      service: "user_details",
      error,
    });
    res.status(500).json({
      success: false,
      message: "Error fetching users with company",
      error: error.message,
    });
  }
};

export const getUsersWithRoleID = async (req, res) => {
  try {
    const pool = await getConnection();
    const result = await pool.request().query(`
      SELECT 
      u.[UserID],
      CONCAT(u.[UserFirstName], ' ', u.[UserLastName]) AS name,
      u.[UserEmail],
      CAST(a.[IsApproved] AS INT) as IsApproved,
      a.[ApprovalType],
      a.[RoleID],
      a.[SurveyProductID],
      a.[SurveyID],
      a.[ConsultantID]
    FROM [dbo].[User] u
    JOIN [dbo].[Approval] a ON u.[UserID] = a.[UserID]
    WHERE a.[ApprovalType] = 'User Role Approval' AND a.[IsApproved] = 1
    ORDER BY u.[UserID] DESC;`);

    Logger.info("Users with role ID fetched successfully", {
      service: "user_details",
    });
    res.status(200).json({
      success: true,
      message: "Users with role ID fetched successfully",
      data: result.recordset, 
    });
  } catch (error) {
    Logger.error("Error fetching users with role ID", {
      service: "user_details",
      error,
    });
    res.status(500).json({
      success: false,
      message: "Error fetching users with role ID",
      error: error.message,
    });
  }
};


export const getUnapprovedUserSignups = async (req, res) => {
  try {
    const pool = await getConnection();
    const result = await pool.request().query(`
      SELECT 
      u.[UserID],
      CONCAT(u.[UserFirstName], ' ', u.[UserLastName]) AS name,
      u.[UserEmail],
      CAST(a.[IsApproved] AS INT) as IsApproved,
      a.[ApprovalType],
      a.[RoleID],
      a.[SurveyProductID],
      a.[SurveyID],
      a.[ConsultantID]
    FROM [dbo].[User] u
    JOIN [dbo].[Approval] a ON u.[UserID] = a.[UserID]
    WHERE a.[ApprovalType] = 'User Signup Approval' AND a.[IsApproved] = 0
    ORDER BY u.[UserID] DESC;`);

    Logger.info("Unapproved user signups fetched successfully", {
      service: "user_details",
    });
    res.status(200).json({
      success: true,
      message: "Unapproved user signups fetched successfully",
      data: result.recordset,
    });
  } catch (error) {
    Logger.error("Error fetching unapproved user signups", {
      service: "user_details",
      error,
    });
    res.status(500).json({
      success: false,
      message: "Error fetching unapproved user signups",
      error: error.message,
    });
  }
};


export const getApprovedCompanyAdminsBySuperAdmin = async (req, res) => {
  try {
    const pool = await getConnection();
    const result = await pool.request().query(`
      SELECT 
      u.[UserID],
      CONCAT(u.[UserFirstName], ' ', u.[UserLastName]) AS name,
      u.[UserEmail],
      CAST(a.[IsApproved] AS INT) as IsApproved,
      a.[ApprovalType],
      a.[RoleID],
      a.[SurveyProductID],
      a.[SurveyID],
      a.[ConsultantID]
    FROM [dbo].[User] u
    JOIN [dbo].[Approval] a ON u.[UserID] = a.[UserID]
    WHERE a.[ApprovalType] = 'User Role Approval' AND a.[RoleID] = 3 AND a.[IsApproved] = 1
    ORDER BY u.[UserID] DESC;
    `);

    Logger.info("Approved users with Role ID fetched successfully", {
      service: "user_details",
    });
    res.status(200).json({
      success: true,
      message: "Approved users with Role ID fetched successfully",
      data: result.recordset, 
    });
  } catch (error) {
    Logger.error("Error fetching approved users with Role ID", {
      service: "user_details",
      error,
    });
    res.status(500).json({
      success: false,
      message: "Error fetching approved users with Role ID",
      error: error.message,
    });
  }
};


export const getApprovedConsultantAdminsBySuperAdmin = async (req, res) => {
  try {
    const pool = await getConnection();
    const result = await pool.request().query(`
      SELECT 
      u.[UserID],
      CONCAT(u.[UserFirstName], ' ', u.[UserLastName]) AS name,
      u.[UserEmail],
      CAST(a.[IsApproved] AS INT) as IsApproved,
      a.[ApprovalType],
      a.[RoleID],
      a.[SurveyProductID],
      a.[SurveyID],
      a.[ConsultantID]
    FROM [dbo].[User] u
    JOIN [dbo].[Approval] a ON u.[UserID] = a.[UserID]
    WHERE a.[ApprovalType] = 'User Role Approval' AND a.[RoleID] = 4 AND a.[IsApproved] = 1
    ORDER BY u.[UserID] DESC;
    `);

    Logger.info("Users with specific role fetched successfully", {
      service: "user_details",
    });
    res.status(200).json({
      success: true,
      message: "Users with specific role fetched successfully",
      data: result.recordset,
    });
  } catch (error) {
    Logger.error("Error fetching users with specific role", {
      service: "user_details",
      error,
    });
    res.status(500).json({
      success: false,
      message: "Error fetching users with specific role",
      error: error.message,
    });
  }
};


export const getCompanyAdminsByConsultant = async (req, res) => {
  const { consultantID } = req.query;

  if (!consultantID) {
    return res.status(400).json({
      success: false,
      message: "ConsultantID is required",
    });
  }

  try {
    const pool = await getConnection();
    const result = await pool.request()
      .input('ConsultantID', consultantID)
      .query(`
        SELECT 
        u.[UserID],
        CONCAT(u.[UserFirstName], ' ', u.[UserLastName]) AS name,
        u.[UserEmail],
        CAST(a.[IsApproved] AS INT) as IsApproved,
        a.[ApprovalType],
        a.[RoleID],
        a.[SurveyProductID],
        a.[SurveyID],
        a.[ConsultantID]
      FROM [dbo].[User] u
      JOIN [dbo].[Approval] a ON u.[UserID] = a.[UserID]
      WHERE a.[ApprovalType] = 'User Role Approval' 
        AND a.[ConsultantID] = @ConsultantID 
        AND a.[RoleID] = 3
        AND a.[IsApproved] = 1
      ORDER BY u.[UserID] DESC;
      `);

    Logger.info("Company admins by consultant fetched successfully", {
      service: "user_details",
    });
    res.status(200).json({
      success: true,
      message: "Company admins by consultant fetched successfully",
      data: result.recordset,
    });
  } catch (error) {
    Logger.error("Error fetching company admins by consultant", {
      service: "user_details",
      error,
    });
    res.status(500).json({
      success: false,
      message: "Error fetching company admins by consultant",
      error: error.message,
    });
  }
};

export const getCompanyUsersByConsultant = async (req, res) => {
  const { consultantID } = req.query;

  if (!consultantID) {
    return res.status(400).json({
      success: false,
      message: "ConsultantID is required",
    });
  }

  try {
    const pool = await getConnection();
    const result = await pool.request()
      .input('ConsultantID', consultantID)
      .query(`
     SELECT 
        u.[UserID],
        CONCAT(u.[UserFirstName], ' ', u.[UserLastName]) AS name,
        u.[UserEmail],
        CAST(a.[IsApproved] AS INT) as IsApproved,
        a.[ApprovalType],
        a.[RoleID],
        a.[SurveyProductID],
        a.[SurveyID],
        a.[ConsultantID]
      FROM [dbo].[User] u
      JOIN [dbo].[Approval] a ON u.[UserID] = a.[UserID]
      WHERE a.[ApprovalType] = 'User Role Approval'
      AND a.[ConsultantID] = @consultantID
      AND a.[RoleID] = 2
      AND a.[IsApproved] = 1
      ORDER BY u.[UserID] DESC;
      `);

    Logger.info("Company users by consultant fetched successfully", {
      service: "user_details",
    });
    res.status(200).json({
      success: true,
      message: "Company users by consultant fetched successfully",
      data: result.recordset,
    });
  } catch (error) {
    Logger.error("Error fetching company users by consultant", {
      service: "user_details",
      error,
    });
    res.status(500).json({
      success: false,
      message: "Error fetching company users by consultant",
      error: error.message,
    });
  }
};

export const getCompaniesByConsultant  = async (req, res) => {
  const { consultantID } = req.query;

  if (!consultantID) {
    return res.status(400).json({
      success: false,
      message: "ConsultantID is required",
    });
  }

  try {
    const pool = await getConnection();
    const result = await pool.request()
      .input('ConsultantID', consultantID)
      .query(`
     SELECT 
        c.[CompanyID],
        c.[CompanyName],
        c.[CompanyType],
        c.[ConsultingCompanyID]
      FROM [dbo].[Company] c
      WHERE c.[ConsultingCompanyID] = @consultantID
      ORDER BY c.[CompanyID] DESC;
      `);

      Logger.info("Companies by consultant fetched successfully", {
        service: "company_details",
      });
      res.status(200).json({
        success: true,
        message: "Companies by consultant fetched successfully",
        data: result.recordset,
      });
    } catch (error) {
      Logger.error("Error fetching companies by consultant", {
        service: "company_details",
        error,
      });
      res.status(500).json({
        success: false,
        message: "Error fetching companies by consultant",
        error: error.message,
      });
    }
  };


  export const getCompanyUsersByCompanyAdmin = async (req, res) => {
    const { adminDomain } = req.query;
  
    if (!adminDomain) {
      return res.status(400).json({
        success: false,
        message: "Missing adminDomain query parameter"
      });
    }
  
    try {
      const pool = await getConnection();
  
      const query = `
        SELECT 
          u.[UserID],
          CONCAT(u.[UserFirstName], ' ', u.[UserLastName]) AS name,
          u.[UserEmail],
          a.[RoleID],
          a.[IsApproved],
          a.[ApprovalType],
          u.[UserCompanyName]
        FROM [dbo].[User] u
        JOIN [dbo].[Approval] a ON u.[UserID] = a.[UserID]
        WHERE a.[ApprovalType] = 'User Role Approval'
        ORDER BY u.[UserID] DESC;
      `;
      
      const result = await pool.request().query(query);
  
      const filteredUsers = result.recordset.filter(user => 
        user.UserCompanyName && 
        user.RoleID === 2 && 
        user.IsApproved === true &&
        user.UserEmail.split('@')[1] === adminDomain
      );
     console.log(filteredUsers)
     
      Logger.info("Filtered users fetched successfully", {
        service: "user_details",
      });
      res.status(200).json({
        success: true,
        message: "Filtered users fetched successfully",
        data: filteredUsers,
      });
    } catch (error) {
      Logger.error("Error fetching filtered users", {
        service: "user_details",
        error,
      });
      res.status(500).json({
        success: false,
        message: "Error fetching filtered users",
        error: error.message,
      });
    }
  };

  export const getAllBuildSurveys = async (req, res) => {
    try {
      const pool = await getConnection();
  
      const query = `
        SELECT 
          [BuildSurveyID],
          [SurveyProductName],
          [SurveyProductCategory],
          [AdditionalContext],
          [SurveyName],
          [QuestionNum],
          [SurveyProductID],
          [Status],
          [CreatedDate],
          [CreatedBy],
          [UpdatedDate],
          [UpdatedBy]
        FROM [dbo].[BuildSurvey] 
        ORDER BY [BuildSurveyID] DESC;
      `;
      const result = await pool.request().query(query);
  
      Logger.info("All build surveys fetched successfully", {
        service: "build_survey_details",
      });
  
      res.status(200).json({
        success: true,
        message: "All build surveys fetched successfully",
        data: result.recordset,
      });
    } catch (error) {
      Logger.error("Error fetching build surveys", {
        service: "build_survey_details",
        error,
      });
  
      res.status(500).json({
        success: false,
        message: "Error fetching build surveys",
        error: error.message,
      });
    }
  };

   export const buildSurveyRequest = async (req, res) => {
    const { SurveyProductName, SurveyProductCategory, SurveyName, AdditionalContext } = req.body;
  
    if (!SurveyProductName || !SurveyProductCategory || !SurveyName) {
      return res.status(400).send('Missing survey data');
    }

    try {
      const pool = await getConnection();
      const query = `
        INSERT INTO [dbo].[BuildSurvey] (
          [SurveyProductName],
          [SurveyProductCategory],
          [AdditionalContext],
          [SurveyName],
          [Status],
          [CreatedDate]
        ) VALUES (
          @SurveyProductName,
          @SurveyProductCategory,
          @AdditionalContext,
          @SurveyName,
          'Requested',
          GETDATE()
        )
      `;
  
      const result = await pool.request()
        .input('SurveyProductName', SurveyProductName)
        .input('SurveyProductCategory', SurveyProductCategory)
        .input('AdditionalContext', AdditionalContext || null)
        .input('SurveyName', SurveyName)
        .query(query);
  
      Logger.info("Survey Request Added successfully", {
        service: "Requested_Survey_Details",
      });
      res.status(200).json({
        success: true,
        message: "Survey Request Added successfully",
        data: result.recordset,
      });
    } catch (error) {
      Logger.error("Error Adding Requested Survey", {
        service: "Requested_Survey_Details",
        error,
      });
      res.status(500).json({
        success: false,
        message: "Error Adding Requested Survey",
        error: error.message,
      });
    }
  };

  export const updateSurveyStatus = async (req, res) => {
    const { BuildSurveyID, Status} = req.body;
  
    if (!BuildSurveyID || !Status) {
      return res.status(400).json({ success: false, message: 'Missing BuildSurveyID or status' });
    }
  
    if (!['Added', 'Reject'].includes(Status)) {
      return res.status(400).json({ success: false, message: 'Invalid status' });
    }
  
    try {
      const pool = await getConnection();
      const query = `
        UPDATE [dbo].[BuildSurvey]
        SET [Status] = @Status
        WHERE [BuildSurveyID] = @BuildSurveyID
      `;
  
      await pool.request()
        .input('BuildSurveyID', BuildSurveyID)
        .input('Status', Status)
        .query(query);
  
      Logger.info("Survey status updated successfully", {
        service: "Survey_Status_Update",
        BuildSurveyID,
        Status,
      });
      res.json({ success: true, message: 'Survey status updated successfully' });
    } catch (error) {
      Logger.error("Error updating survey status", {
        service: "Survey_Status_Update",
        error,
      });
      res.status(500).json({ success: false, message: 'Error updating survey status', error: error.message });
    }
  };


  export const deleteBuildSurvey = async (req, res) => {
    const { BuildSurveyID, SurveyName, Status } = req.body;
  
    if (!BuildSurveyID || !SurveyName || !Status) {
      return res.status(400).json({
        success: false,
        message: 'BuildSurveyID, SurveyName, and Status are required',
      });
    }
  
    if (Status === 'InProgress' || Status === 'Started') {
      return res.status(200).json({
        success: true,
        message: 'Survey cannot be deleted in its current state',
      });
    }
  
    const pool = await getConnection();
  
    try {
      if (Status === 'Added' || Status === 'Requested' || Status === 'Reject') {
        try {
          await pool.request()
            .input('BuildSurveyID', BuildSurveyID)
            .query(`
              DELETE FROM [dbo].[BuildSurvey]
              WHERE [BuildSurveyID] = @BuildSurveyID
            `);
          Logger.info("BuildSurvey record deleted successfully", {
            service: "survey_deletion",
          });
        } catch (error) {
          Logger.error("Error deleting BuildSurvey record", {
            service: "survey_deletion",
            error,
          });
        }
  
        res.status(200).json({
          success: true,
          message: 'BuildSurvey record deleted successfully',
        });
      } else if (Status === 'Completed' || Status === 'Failed') {
        try {
          // Step 1: Delete from BuildSurvey
          await pool.request()
            .input('BuildSurveyID', BuildSurveyID)
            .query(`
              DELETE FROM [dbo].[BuildSurvey]
              WHERE [BuildSurveyID] = @BuildSurveyID
            `);
        } catch (error) {
          Logger.error("Error deleting BuildSurvey record", {
            service: "survey_deletion",
            error,
          });
        }
  
        try {
          // Step 2: Get SurveyID and SurveyProductID based on SurveyName
          const surveyResult = await pool.request()
            .input('SurveyName', SurveyName)
            .query(`
              SELECT [SurveyID],[SurveyProductID]
              FROM [dbo].[Survey]
              WHERE [SurveyName] = @SurveyName
            `);
  
          if (surveyResult.recordset.length === 0) {
            return res.status(404).json({
              success: false,
              message: 'Survey not found',
            });
          }
  
          const SurveyID = surveyResult.recordset[0].SurveyID;
          const SurveyProductID = surveyResult.recordset[0].SurveyProductID;
  
          try {
            // Step 3: Delete responses associated with the questions
            await pool.request()
              .input('SurveyID', SurveyID)
              .query(`
                DELETE FROM [dbo].[Response]
                WHERE [ResponseQuestionID] IN (
                  SELECT [QuestionID]
                  FROM [dbo].[Question]
                  WHERE [QuestionSurveyID] = @SurveyID
                )
              `);
          } catch (error) {
            Logger.error("Error deleting responses", {
              service: "survey_deletion",
              error,
            });
          }
  
          try {
            // Step 4: Delete questions associated with the survey
            await pool.request()
              .input('SurveyID', SurveyID)
              .query(`
                DELETE FROM [dbo].[Question]
                WHERE [QuestionSurveyID] = @SurveyID
              `);
          } catch (error) {
            Logger.error("Error deleting questions", {
              service: "survey_deletion",
              error,
            });
          }
  
          try {
            // Step 5: Delete from Survey
            await pool.request()
              .input('SurveyName', SurveyName)
              .query(`
                DELETE FROM [dbo].[Survey]
                WHERE [SurveyName] = @SurveyName
              `);
          } catch (error) {
            Logger.error("Error deleting survey", {
              service: "survey_deletion",
              error,
            });
          }
  
          try {
            // Step 6: Delete from SurveyGroup
            await pool.request()
              .input('SurveyProductID', SurveyProductID)
              .query(`
                DELETE FROM [dbo].[SurveyGroup]
                WHERE [SurveyProductID] = @SurveyProductID
              `);
          } catch (error) {
            Logger.error("Error deleting survey group", {
              service: "survey_deletion",
              error,
            });
          }
  
          Logger.info("Survey deleted successfully", {
            service: "survey_deletion",
          });
  
          res.status(200).json({
            success: true,
            message: 'Survey deleted successfully',
          });
        } catch (error) {
          Logger.error("Error deleting survey", {
            service: "survey_deletion",
            error,
          });
  
          res.status(500).json({
            success: false,
            message: 'Error deleting survey',
            error: error.message,
          });
        }
      } else {
        return res.status(400).json({
          success: false,
          message: 'Invalid survey status',
        });
      }
    } catch (error) {
      Logger.error("Error deleting survey", {
        service: "survey_deletion",
        error,
      });
  
      res.status(500).json({
        success: false,
        message: 'Error deleting survey',
        error: error.message,
      });
    }
  };

  export const createUser = async (req, res) => {
    const { userEmail } = req.body;
  
    if (!userEmail) {
      return res.status(400).json({
        success: false,
        message: 'User email is required',
      });
    }
  
    const managementApiEndpoint = `${process.env.AUTH0_DOMAIN}/api/v2/users`;
    const userPassword = generateRandomPassword(12);
    try {
      const accessToken = await getAccessToken();
      const response = await axios.post(managementApiEndpoint, {
        email: userEmail,
        password: userPassword,
        connection: 'Username-Password-Authentication',
      }, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });
      Logger.info('User created successfully', {
        service: 'user_creation',
        userEmail,
      });
  
      res.status(200).json({
        success: true,
        data: response.data,
        password: userPassword,
      });
    } catch (error) {
      Logger.error('Error creating user', {
        service: 'user_creation',
        error,
      });
  
      res.status(500).json({
        success: false,
        message: 'Error creating user',
        error: error.message,
      });
    }
  };


export const deleteUserFromAuth0 = async (req, res) => {
  const { AuthId } = req.body;

  if (!AuthId) {
    return res.status(400).json({
      success: false,
      message: 'Auth0 ID (authId) is required',
    });
  }

  const managementApiEndpoint = `${process.env.AUTH0_DOMAIN}/api/v2/users/${AuthId}`;

  let pool;
  try {
    pool = await getConnection(); // Ensure this returns a valid connection pool

    // Step 1: Find the UserID from the User table
    const userResult = await pool.request()
      .input('AuthId', AuthId)
      .query(`
        SELECT [UserID]
        FROM [dbo].[User]
        WHERE [AuthId] = @AuthId;
      `);

    if (userResult.recordset.length === 0) {
      return res.status(404).json({
        success: false,
        message: 'User not found',
      });
    }

    const userId = userResult.recordset[0].UserID;

    // Step 2: Delete related records from the Approval table
    try {
      await pool.request()
        .input('UserId',userId)
        .query(`
          DELETE FROM [dbo].[Approval]
          WHERE [UserID] = @UserId;
        `);
    } catch (approvalError) {
      Logger.error('Error deleting related records from Approval table', {
        service: 'user_deletion',
        approvalError,
      });
      return res.status(500).json({
        success: false,
        message: 'Error deleting related records',
        error: approvalError.message,
      });
    }

    // Step 3: Delete the user from the User table
    try {
      await pool.request()
        .input('UserId', userId)
        .query(`
          DELETE FROM [dbo].[User]
          WHERE [UserID] = @UserId;
        `);
    } catch (userError) {
      Logger.error('Error deleting user from User table', {
        service: 'user_deletion',
        userError,
      });
      return res.status(500).json({
        success: false,
        message: 'Error deleting user',
        error: userError.message,
      });
    }

    // Step 4: Delete the user from Auth0
    try {
      const accessToken = await getAccessToken();
      await axios.delete(managementApiEndpoint, {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      });
    } catch (auth0Error) {
      Logger.error('Error deleting user from Auth0', {
        service: 'user_deletion',
        auth0Error,
      });
      return res.status(500).json({
        success: false,
        message: 'Error deleting user from Auth0',
        error: auth0Error.message,
      });
    }

    Logger.info('User and related records deleted successfully', {
      service: 'user_deletion',
      AuthId,
    });

    res.status(200).json({
      success: true,
      message: 'User and related records deleted successfully',
    });
  } catch (error) {
    Logger.error('Unexpected error during user deletion process', {
      service: 'user_deletion',
      error,
    });

    res.status(500).json({
      success: false,
      message: 'Unexpected error during user deletion process',
      error: error.message,
    });
  }
};

// export const updateUserApproval = async (req, res) => {
//   try {
//     const { UserID, IsApproved, RoleID,SurveyID,SurveyProductID } = req.body;

//     if (typeof UserID !== 'number') {
//       return res.status(400).json({
//         success: false,
//         message: 'Invalid input data',
//       });
//     }

//     // if (typeof IsApproved !== 'number' && typeof RoleID !== 'number' && typeof SurveyID !== 'number' && typeof SurveyProductID !== 'number') {
//     //   return res.status(400).json({
//     //     success: false,
//     //     message: 'Either IsApproved or RoleID must be provided',
//     //   });
//     // }

//     const pool = await getConnection();

//     let query = `UPDATE [dbo].[Approval] SET `;
//     const inputs = [];
//     if (typeof IsApproved === 'number') {
//       query += `[IsApproved] = @IsApproved`;
//       inputs.push({ name: 'IsApproved', value: IsApproved });
//     }
//     if (typeof RoleID === 'number') {
//       if (inputs.length > 0) query += `, `;
//       query += `[RoleID] = @RoleID WHERE [ApprovalType] = 'User Role Approval'`;
//       inputs.push({ name: 'RoleID', value: RoleID });
//     }
//     if (typeof SurveyID === 'number') {
//       if (inputs.length > 0) query += `, `;
//       query += `[SurveyID] = @SurveyID`;
//       inputs.push({ name: 'SurveyID', value: SurveyID });
//     }
//     if (typeof SurveyProductID === 'number') {
//       if (inputs.length > 0) query += `, `;
//       query += `[SurveyProductID] = @SurveyProductID`;
//       inputs.push({ name: 'SurveyProductID', value: SurveyProductID });
//     }
//     query += ` WHERE [UserID] = @UserID`;

//     const request = pool.request().input('UserID', UserID);
//     inputs.forEach(input => {
//       request.input(input.name, input.value);
//     });

//     const result = await request.query(query);

//     Logger.info("User approval updated successfully", { service: 'user_details' });

//     if (result.rowsAffected[0] === 0) {
//       return res.status(404).json({
//         success: false,
//         message: 'User not found or no changes made',
//       });
//     }

//     const responseData = { UserID };
//     if (typeof IsApproved === 'number') responseData.IsApproved = IsApproved;
//     if (typeof RoleID === 'number') responseData.RoleID = RoleID;
//     if (typeof SurveyID === 'number') responseData.SurveyID = SurveyID;
//     if (typeof SurveyProductID === 'number') responseData.SurveyProductID = SurveyProductID;

//     res.status(200).json({
//       success: true,
//       message: "User approval updated successfully",
//       data: responseData,
//     });
//   } catch (error) {
//     Logger.error("Error updating user approval", { service: 'user_details', error });

//     res.status(500).json({
//       success: false,
//       message: 'Internal server error',
//     });
//   }
// };

export const updateUserApproval = async (req, res) => {
  try {
    const { UserID, RoleID, IsApproved } = req.body;

    // Validate the input data
    if (
      typeof UserID !== "number" ||
      (IsApproved !== undefined && typeof IsApproved !== "number") ||
      (RoleID !== undefined && typeof RoleID !== "number")
    ) {
      return res.status(400).json({
        success: false,
        message: "Invalid input data",
      });
    }

    const pool = await getConnection();

    const userResult = await pool.request().input("UserID", UserID).query(`
      SELECT [UserEmail] FROM [dbo].[User] WHERE [UserID] = @UserID
    `);

    if (userResult.recordset.length === 0) {
      return res.status(404).json({
        success: false,
        message: "User not found",
      });
    }
    const UserEmail = userResult.recordset[0].UserEmail;

    let updateIsApprovalResult = null;
    let updateRoleIDResult = null;

    let isApprovalUpdated = false;
    let roleIDUpdated = false;

    // Update IsApproved if provided
    if (IsApproved !== undefined) {
      updateIsApprovalResult = await pool.request()
        .input("UserID", UserID)
        .input("IsApproved", IsApproved)
        .query(`
          UPDATE [dbo].[Approval]
          SET [IsApproved] = @IsApproved
          WHERE [UserID] = @UserID;
        `);

      if (updateIsApprovalResult.rowsAffected[0] > 0 && IsApproved === 1) {
        isApprovalUpdated = true;
        const emailTemplateContext = {
          email: UserEmail,
        };
        const mailOptionsApprovalUpdate = {
          from: "mallalokesh23@gmail.com",
          to: UserEmail,
          subject: "Your EVA Account Has Been Approved",
          html: `<!DOCTYPE html>
                  <html lang="en">
                  <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Account Approved - EVA</title>
                    <style>
                      body {
                        font-family: Arial, sans-serif;
                        background-color: #f4f4f4;
                        margin: 0;
                        padding: 0;
                        color: #333;
                      }
                      .container {
                        max-width: 600px;
                        margin: 50px auto;
                        background-color: #fff;
                        padding: 20px;
                        border-radius: 8px;
                        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                      }
                      .header {
                        text-align: center;
                        background-color: #212121;
                        border-bottom: 1px solid #e6e6e6;
                        padding-bottom: 20px;
                        margin-bottom: 20px;
                      }
                      .header img {
                        margin-top: 10px;
                        max-width: 50px;
                        width: 20%;
                        display: inline-block;
                        vertical-align: middle;
                      }
                      .header h1 {
                        font-size: 24px;
                        color: #fff;
                        margin: 0;
                      }
                      .content {
                        text-align: center;
                      }
                      .content h2 {
                        font-size: 20px;
                        color: #333;
                        margin: 0 0 20px;
                      }
                      .content p {
                        font-size: 16px;
                        color: #666;
                        line-height: 1.6;
                      }
                      .btn-link {
                        background-color: #002984;
                        color: #ffffff !important;
                        text-decoration: none;
                        border-radius: 5px;
                        padding: 10px 20px;
                        display: inline-block;
                      }
                      .btn-link:hover {
                        background-color: #001c6c;
                        color: #ffffff !important;
                      }
                      .footer {
                        text-align: center;
                        padding-top: 20px;
                        border-top: 1px solid #e6e6e6;
                        margin-top: 20px;
                        color: #999;
                        font-size: 14px;
                      }
                    </style>
                  </head>
                  <body>
                    <div class="container">
                      <div class="header">
                        <img src="https://coeindex0storage.blob.core.windows.net/file-uploads/EVA%20LOGO.png" alt="Logo" style="margin-top: 10px;">
                        <h1>Account Approved - EVA</h1>
                      </div>
                      <div class="content">
                        <h2>Hello, ${emailTemplateContext.email}</h2>
                        <p>We are pleased to inform you that your account with EVA has been approved.</p>
                        <p>You can now log in with your login credentials</p>
                        <p>Please use the link below to log in:</p>
                        <a href="https://app.evaomax.com/" class="btn-link">Log In</a>
                      </div>
                      <div class="footer">
                        <p>If you have any questions or need assistance, feel free to contact us at EVA@CloudOMax.com.</p>
                        <p>Thank you!</p>
                      </div>
                    </div>
                  </body>
                  </html>`,
          context: emailTemplateContext,
        };
        await sendInvitationEmail(mailOptionsApprovalUpdate, emailTemplateContext);
      }
    }

    // Update RoleID if provided
    if (RoleID !== undefined) {
      const userRoleResult = await pool.request()
        .input("RoleID", RoleID)
        .query(`
          SELECT [RoleName] FROM [dbo].[Roles] WHERE [RoleID] = @RoleID
        `);

      const userRoleName = userRoleResult.recordset[0]?.RoleName;

      updateRoleIDResult = await pool.request()
        .input("UserID", UserID)
        .input("RoleID", RoleID)
        .query(`
          UPDATE [dbo].[Approval]
          SET [RoleID] = @RoleID
          WHERE [UserID] = @UserID AND [ApprovalType] = 'User Role Approval';
        `);

      if (updateRoleIDResult.rowsAffected[0] > 0) {
        roleIDUpdated = true;
        const emailTemplateContext = {
          email: UserEmail,
          newRole: userRoleName
        };
        const mailOptionsRoleUpdate = {
          from: "Bhargav.Konathala@cloudomaxcorp.com",
          to: UserEmail,
          subject: 'Your Role in EVA Has Been Updated',
          html: `<!DOCTYPE html>
                  <html lang="en">
                  <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Role Update - EVA</title>
                    <style>
                      body {
                        font-family: Arial, sans-serif;
                        background-color: #f4f4f4;
                        margin: 0;
                        padding: 0;
                        color: #333;
                      }
                      .container {
                        max-width: 600px;
                        margin: 50px auto;
                        background-color: #fff;
                        padding: 20px;
                        border-radius: 8px;
                        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                      }
                      .header {
                        text-align: center;
                        background-color: #212121;
                        border-bottom: 1px solid #e6e6e6;
                        padding-bottom: 20px;
                        margin-bottom: 20px;
                      }
                      .header img {
                        margin-top: 10px;
                        max-width: 50px;
                        width: 20%;
                        display: inline-block;
                        vertical-align: middle;
                      }
                      .header h1 {
                        font-size: 24px;
                        color: #fff;
                        margin: 0;
                      }
                      .content {
                        text-align: center;
                      }
                      .content h2 {
                        font-size: 20px;
                        color: #333;
                        margin: 0 0 20px;
                      }
                      .content p {
                        font-size: 16px;
                        color: #666;
                        line-height: 1.6;
                      }
                      .btn-link {
                        background-color: #002984;
                        color: #ffffff !important;
                        text-decoration: none;
                        border-radius: 5px;
                        padding: 10px 20px;
                        display: inline-block;
                      }
                      .btn-link:hover {
                        background-color: #001c6c;
                        color: #ffffff !important;
                      }
                      .footer {
                        text-align: center;
                        padding-top: 20px;
                        border-top: 1px solid #e6e6e6;
                        margin-top: 20px;
                        color: #999;
                        font-size: 14px;
                      }
                    </style>
                  </head>
                  <body>
                    <div class="container">
                      <div class="header">
                        <img src="https://coeindex0storage.blob.core.windows.net/file-uploads/EVA%20LOGO.png" alt="Logo" style="margin-top: 10px;">
                        <h1>Role Update - EVA</h1>
                      </div>
                      <div class="content">
                        <h2>Hello, ${emailTemplateContext.email}</h2>
                        <p>We are pleased to inform you that your role within EVA has been updated.</p>
                        <p>Your new role is: <strong>${emailTemplateContext.newRole}</strong></p>
                        <p>Please use the link below to log in:</p>
                        <a href="https://app.evaomax.com/" class="btn-link">Log In</a>
                      </div>
                      <div class="footer">
                        <p>If you have any questions or need assistance, feel free to contact us at EVA@CloudOMax.com.</p>
                        <p>Thank you!</p>
                      </div>
                    </div>
                  </body>
                  </html>`,
          context: emailTemplateContext,
        };
        await sendInvitationEmail(mailOptionsRoleUpdate, emailTemplateContext);
      }
    }

    // Check if any updates were made
    if (!isApprovalUpdated && !roleIDUpdated) {
      return res.status(404).json({
        success: false,
        message: "No changes made",
      });
    }

    // Respond based on what was updated
    if (isApprovalUpdated) {
      return res.status(200).json({
        success: true,
        message: "User approval status updated successfully",
      });
    }

    if (roleIDUpdated) {
      return res.status(200).json({
        success: true,
        message: "Role ID updated successfully",
      });
    }

  } catch (error) {
    Logger.error("Error updating user approval", {
      service: "user_details",
      error,
    });
    res.status(500).json({
      success: false,
      message: "Internal server error",
    });
  }
};


//     // Update IsApproved if provided
//     if (IsApproved !== undefined) {
//       updateIsApprovalResult = await pool
//         .request()
//         .input("UserID", UserID)
//         .input("IsApproved", IsApproved).query(`
//           UPDATE [dbo].[Approval]
//           SET [IsApproved] = @IsApproved
//           WHERE [UserID] = @UserID;
//         `);

//       Logger.info("User approval status updated successfully", {
//         service: "user_details",
//       });
//       // Send email if approval status is updated
//       if (updateIsApprovalResult.rowsAffected[0] > 0 && IsApproved === 1) {
//         const mailOptions = {
//           from: "your-email@gmail.com",
//           to: UserEmail,
//           subject: "Your EVA Account Has Been Approved",
//           html: `<!DOCTYPE html>
//                   <html lang="en">
//                   <head>
//                     <meta charset="UTF-8">
//                     <meta http-equiv="X-UA-Compatible" content="IE=edge">
//                     <meta name="viewport" content="width=device-width, initial-scale=1.0">
//                     <title>Account Approved - EVA</title>
//                     <style>
//                       body {
//                         font-family: Arial, sans-serif;
//                         background-color: #f4f4f4;
//                         margin: 0;
//                         padding: 0;
//                         color: #333;
//                       }
//                       .container {
//                         max-width: 600px;
//                         margin: 50px auto;
//                         background-color: #fff;
//                         padding: 20px;
//                         border-radius: 8px;
//                         box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
//                       }
//                       .header {
//                         text-align: center;
//                         background-color: #212121;
//                         border-bottom: 1px solid #e6e6e6;
//                         padding-bottom: 20px;
//                         margin-bottom: 20px;
//                       }
//                       .header img {
//                         margin-top: 10px;
//                         max-width: 50px;
//                         width: 20%;
//                         display: inline-block;
//                         vertical-align: middle;
//                       }
//                       .header h1 {
//                         font-size: 24px;
//                         color: #fff;
//                         margin: 0;
//                       }
//                       .content {
//                         text-align: center;
//                       }
//                       .content h2 {
//                         font-size: 20px;
//                         color: #333;
//                         margin: 0 0 20px;
//                       }
//                       .content p {
//                         font-size: 16px;
//                         color: #666;
//                         line-height: 1.6;
//                       }
//                       .btn-link {
//                         background-color: #002984;
//                         color: #ffffff !important;
//                         text-decoration: none;
//                         border-radius: 5px;
//                         padding: 10px 20px;
//                         display: inline-block;
//                       }

//                       .btn-link:hover {
//                         background-color: #001c6c;
//                         color: #ffffff !important;
//                       }
//                       .footer {
//                         text-align: center;
//                         padding-top: 20px;
//                         border-top: 1px solid #e6e6e6;
//                         margin-top: 20px;
//                         color: #999;
//                         font-size: 14px;
//                       }
//                     </style>
//                   </head>
//                   <body>
//                     <div class="container">
//                       <div class="header">
//                         <img src="https://coeindex0storage.blob.core.windows.net/file-uploads/EVA%20LOGO.png" alt="Logo" style="margin-top: 10px;">
//                         <h1>Account Approved - EVA</h1>
//                       </div>
//                       <div class="content">
//                         <h2>Hello, ${UserEmail}</h2>
//                         <p>We are pleased to inform you that your account with EVA has been approved.</p>
//                         <p>You can now log in with your login credentials</p>
//                         <p>Please use the link below to log in:</p>
//                         <a href="https://app.evaomax.com/" class="btn-link">Log In</a>
//                       </div>
//                       <div class="footer">
//                         <p>If you have any questions or need assistance, feel free to contact us at EVA@CloudOMax.com.</p>
//                         <p>Thank you!</p>
//                       </div>
//                     </div>
//                   </body>
//                   </html>`,
//         };

//         transporter.sendMail(mailOptions, (error, info) => {
//           if (error) {
//             Logger.error("Error sending approval email", { error });
//           } else {
//             Logger.info("Approval email sent: " + info.response);
//           }
//         });
//       }
//     }

//     // Update RoleID if provided
//     if (RoleID !== undefined) {
//       updateRoleIDResult = await pool
//         .request()
//         .input("UserID", UserID)
//         .input("RoleID", RoleID).query(`
//           UPDATE [dbo].[Approval]
//           SET [RoleID] = @RoleID
//           WHERE [UserID] = @UserID AND [ApprovalType] = 'User Role Approval';
//         `);

//       Logger.info("Role ID updated successfully", { service: "user_details" });
//     }

//     // Check if any updates were made
//     if (
//       (updateIsApprovalResult && updateIsApprovalResult.rowsAffected[0] === 0) &&
//       (updateRoleIDResult && updateRoleIDResult.rowsAffected[0] === 0)
//     ) {
//       return res.status(404).json({
//         success: false,
//         message: "User not found or no changes made",
//       });
//     }

//     res.status(200).json({
//       success: true,
//       message: "User approval updated successfully",
//     });
//   } catch (error) {
//     Logger.error("Error updating user approval", {
//       service: "user_details",
//       error,
//     });
//     res.status(500).json({
//       success: false,
//       message: "Internal server error",
//     });
//   }
// };

    

export const insertNewRecord = async (req, res) => {
  try {
    const { UserID } = req.body;
    console.log("Received UserId:", UserID);

    const pool = await getConnection();
    console.log("Database connection established.");

    // Define the required approval types
    const requiredApprovalTypes = [
      "User Survey Group Approval",
      "User Individual Survey Approval",
      "Company Survey Group Approval",
      "Company Individual Survey Approval",
      "User is Consulting to Company",
      "User Signup Approval",
      "User Role Approval",
    ];

    // Log the SQL query and parameters
    const query = `
      SELECT [ApprovalType] FROM [dbo].[Approval]
      WHERE [UserID] = @UserID AND [ApprovalType] IN 
      ('User Survey Group Approval', 'User Individual Survey Approval', 'Company Survey Group Approval', 
       'Company Individual Survey Approval', 'User is Consulting to Company', 'User Signup Approval', 'User Role Approval')
    `;
    console.log("Executing query:", query);
    console.log("With parameters:", { UserID });

    // Query existing approval types for the user
    const existingApprovalTypesResult = await pool
      .request()
      .input("UserID", UserID)
      .query(query);

    // Log the raw query result to debug the issue
    console.log("Raw Query Result:", existingApprovalTypesResult);

    // Map the results to get the list of existing approval types
    const existingApprovalTypes = existingApprovalTypesResult.recordset.map(
      (record) => record.ApprovalType
    );
    console.log("Existing Approval Types:", existingApprovalTypes);

    // Determine the missing approval types
    const missingApprovalTypes = requiredApprovalTypes.filter(
      (type) => !existingApprovalTypes.includes(type)
    );
    console.log("Missing Approval Types for User:", missingApprovalTypes);

    return res.status(200).json({
      success: true,
      message: "Checked Approval Types",
      existingApprovalTypes: existingApprovalTypes,
      missingApprovalTypes: missingApprovalTypes,
    });
  } catch (error) {
    console.error("Error processing request:", error);
    return res.status(500).json({
      success: false,
      message: "Internal Server Error",
    });
  }
};

export const getCompanies = async (req, res) => {
  try {
    const pool = await getConnection();
    const result = await pool.request().query(`
      SELECT * FROM [dbo].[Company]
    `);

    const emp = result.recordset;

    if (emp && emp.length > 0) {
      Logger.info("The Company details were fetched successfully", {
        service: "Company_details",
      });
      res.status(200).json({
        success: true,
        message: "The Company details were fetched successfully",
        data: emp,
      });
    } else {
      Logger.info("No Company details were found", {
        service: "Company_details",
      });
      res.status(404).json({
        success: false,
        message: "No Company details were found",
      });
    }
  } catch (error) {
    console.error(error);
    Logger.error("Company fetch failed", { service: "Company_details", error });
    res.status(500).json({
      success: false,
      message: "Company fetch failed",
      error: error.message,
    });
  }
};

// export const insertRecordsByCompany = async (req, res) => {
//   try {
//     const { CompanyID, SurveyProductID, SurveyID,ApprovalType } = req.body;

//     // Validate required fields
//     if (!CompanyID ||  !ApprovalType) {
//       return res.status(400).json({
//         success: false,
//         message: "CompanyID, SurveyProductID, SurveyID, IsApproved, ApprovedTillDate, and ApprovalType are required fields"
//       });
//     }

//     // Validate ApprovalType
//     const validApprovalTypes = ["Company Survey Group Approval", "Company Individual Survey Approval"];
//     if (!validApprovalTypes.includes(ApprovalType)) {
//       return res.status(400).json({ error: 'Invalid ApprovalType. Must be "Company Survey Group Approval" or "Company Individual Survey Approval"' });
//     }
//     let additionalValidationPassed = true;
//     if(ApprovalType === "Company Survey Group Approval"){
//       if(!SurveyProductID){
//         additionalValidationPassed = false;

//       }
//     }else if(ApprovalType === "Company Individual Survey Approval"){
//       if(!SurveyID){
//         additionalValidationPassed = false;
//       }

//     }

//     if(!additionalValidationPassed){
//       return res.status(400).json({
//         success:false,
//         message:"SurveryProductID is require for Company Survey Group Approval,SurveyID is requiered for Company Individual Survey Approval"
//       })
//     }

//     // Get current date for ApprovedTillDate
//     const approvedTillDate = '2024-12-31 00:00:00.000';
//     const IsApproved = 1;

//     // Execute SQL query to insert record
//     const pool = await getConnection(); // Assuming getConnection() function is defined elsewhere
//     const query = `
//       INSERT INTO [dbo].[Approval] (CompanyID, SurveyProductID, SurveyID, IsApproved, ApprovedTillDate, ApprovalType)
//       VALUES (@CompanyID, @SurveyProductID, @SurveyID, @IsApproved, @ApprovedTillDate, @ApprovalType)
//     `;
//     const result = await pool.request()
//       .input('CompanyID', CompanyID)
//       .input('SurveyProductID', SurveyProductID || null)
//       .input('SurveyID', SurveyID || null)
//       .input('IsApproved', IsApproved)
//       .input('ApprovedTillDate', approvedTillDate)
//       .input('ApprovalType', ApprovalType)
//       .query(query);

//     console.log('Record inserted successfully:', result);

//     // Respond with success message
//     return res.status(200).json({ success: true, message: 'Record inserted successfully' });

//   } catch (error) {
//     console.error('Error:', error);
//     return res.status(500).json({ error: 'Internal server error'});
//  }
// };

export const insertRecordsByCompany = async (req, res) => {
  try {
    const { CompanyID, SurveyProductID, SurveyID, ApprovalType } = req.body;

    // Validate required fields
    if (!CompanyID || !ApprovalType) {
      return res.status(400).json({
        success: false,
        message: "CompanyID and ApprovalType are required fields",
      });
    }

    // Validate ApprovalType
    const validApprovalTypes = [
      "Company Survey Group Approval",
      "Company Individual Survey Approval",
    ];
    if (!validApprovalTypes.includes(ApprovalType)) {
      return res.status(400).json({
        success: false,
        message:
          'Invalid ApprovalType. Must be "Company Survey Group Approval" or "Company Individual Survey Approval"',
      });
    }

    // Additional validation based on ApprovalType
    if (
      (ApprovalType === "Company Survey Group Approval" && !SurveyProductID) ||
      (ApprovalType === "Company Individual Survey Approval" && !SurveyID)
    ) {
      return res.status(400).json({
        success: false,
        message:
          "SurveyProductID is required for Company Survey Group Approval, SurveyID is required for Company Individual Survey Approval",
      });
    }

    // Database connection
    const pool = await getConnection();

    // Check if CompanyID exists in the database
    const checkCompanyIdQuery = `
      SELECT TOP 1 1 FROM [dbo].[Company] WHERE [CompanyID] = @CompanyID
    `;
    const checkCompanyIdResult = await pool
      .request()
      .input("CompanyID", CompanyID)
      .query(checkCompanyIdQuery);

    if (checkCompanyIdResult.recordset.length === 0) {
      return res.status(400).json({
        success: false,
        message: "CompanyID does not exist in the database",
      });
    }

    // Check if SurveyID or SurveyProductID already exists for the given CompanyID
    const checkQuery = `
      SELECT TOP 1 1 FROM [dbo].[Approval] 
      WHERE [CompanyID] = @CompanyID AND 
            (@SurveyID IS NULL OR [SurveyID] = @SurveyID) AND 
            (@SurveyProductID IS NULL OR [SurveyProductID] = @SurveyProductID)
    `;
    const checkResult = await pool
      .request()
      .input("CompanyID", CompanyID)
      .input("SurveyID", SurveyID || null)
      .input("SurveyProductID", SurveyProductID || null)
      .query(checkQuery);

    if (checkResult.recordset.length > 0) {
      return res.status(200).json({
        success: false,
        message:
          "SurveyID or SurveyProductID is already assigned to the specified CompanyID",
      });
    }

    // Proceed to insert the record
    const approvedTillDate = "2024-12-31 00:00:00.000";
    const IsApproved = 1;
    const insertQuery = `
      INSERT INTO [dbo].[Approval] (CompanyID, SurveyProductID, SurveyID, IsApproved, ApprovedTillDate, ApprovalType)
      VALUES (@CompanyID, @SurveyProductID, @SurveyID, @IsApproved, @ApprovedTillDate, @ApprovalType)
    `;
    const insertResult = await pool
      .request()
      .input("CompanyID", CompanyID)
      .input("SurveyProductID", SurveyProductID || null)
      .input("SurveyID", SurveyID || null)
      .input("IsApproved", IsApproved)
      .input("ApprovedTillDate", approvedTillDate)
      .input("ApprovalType", ApprovalType)
      .query(insertQuery);

    console.log("Record inserted successfully:", insertResult);
    return res
      .status(200)
      .json({ success: true, message: "Record inserted successfully" });
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({ error: "Internal server error" });
  }
};

export const insertRecordByUserid = async (req, res) => {
  try {
    const { UserID, SurveyProductID, SurveyID, ApprovalType } = req.body;
    if (!UserID && !ApprovalType) {
      return res.status(400).json({
        success: false,
        message: "UserID and ApprovalType  are required fields",
      });
    }
    const validApprovalTypes = [
      "User Individual Survey Approval",
      "User Survey Group Approval",
    ];
    if (!validApprovalTypes.includes(ApprovalType)) {
      return res.status(400).json({
        error:
          'Invalid ApprovalType. Must be "User Individual Survey Approval" or "User Survey Group Approval"',
      });
    }
    let additionalValidationPassed = true;
    if (ApprovalType === "User Individual Survey Approval" && !SurveyID) {
      additionalValidationPassed = false;
    } else if (
      ApprovalType === "User Survey Group Approval" &&
      !SurveyProductID
    ) {
      additionalValidationPassed = false;
    }
    if (!additionalValidationPassed) {
      return res.status(400).json({
        success: false,
        message:
          "SurveyProductID is required for User Survey Group Approval, SurveyID is required for User Individual Survey Approval",
      });
    }

    const pool = await getConnection();
    const cheeckUserId = `
      SELECT TOP 1 1 FROM [dbo].[Approval] WHERE [UserID] = @UserID
    `;
    const cheeckUserIdResult = await pool
      .request()
      .input("UserID", UserID)
      .query(cheeckUserId);
    if (cheeckUserIdResult.recordset.length === 0) {
      return res.status(400).json({
        success: false,
        message: "UserID not Exist in Database",
      });
    }
    
        // Get the userEmail associated with the UserID
        const getUserEmailQuery = `
        SELECT UserEmail FROM [dbo].[User] WHERE [UserID] = @UserID
      `;
      const userEmailResult = await pool
        .request()
        .input("UserID", UserID)
        .query(getUserEmailQuery);
  
      const userEmail = userEmailResult.recordset[0].UserEmail;

    // Check if SurveyID or SurveyProductID already exists for the given UserID

    const checkQuery = `
      SELECT TOP 1 1 FROM [dbo].[Approval] WHERE [UserID] = @UserID AND ([SurveyID] = @SurveyID OR [SurveyProductID] = @SurveyProductID) 
    `;
    const checkResult = await pool
      .request()
      .input("UserID", UserID)
      .input("SurveyID", SurveyID || null)
      .input("SurveyProductID", SurveyProductID || null)
      .query(checkQuery);

    // If SurveyID or SurveyProductID already exists for the given UserID, return an error
    if (checkResult.recordset.length > 0) {
      return res.status(200).json({
        success: false,
        message:
          "SurveyID or SurveyProductID is already assigned to the specified UserID",
      });
    }

    // Proceed to insert the record
    const approvedTillDate = "2024-12-31 00:00:00.000";
    const IsApproved = 1;
    const insertQuery = `
      INSERT INTO [dbo].[Approval] (UserID, SurveyProductID, SurveyID, IsApproved, ApprovedTillDate, ApprovalType)
      VALUES (@UserID, @SurveyProductID, @SurveyID, @IsApproved, @ApprovedTillDate, @ApprovalType)
    `;
    const insertResult = await pool
      .request()
      .input("UserID", UserID)
      .input("SurveyProductID", SurveyProductID || null)
      .input("SurveyID", SurveyID || null)
      .input("IsApproved", IsApproved)
      .input("ApprovedTillDate", approvedTillDate)
      .input("ApprovalType", ApprovalType)
      .query(insertQuery);

    // Send email to user with survey details
    const surveyDetails = await getSurveyDetails(SurveyID, SurveyProductID, ApprovalType);

    let emailTemplateContext;
    if (ApprovalType === "User Individual Survey Approval") {
      emailTemplateContext = {
        userEmail: userEmail,
        surveyName: surveyDetails.SurveyName,
        surveyDescription: surveyDetails.SurveyDesc,
        startTime: surveyDetails.SurveyStartDate,
        endTime: surveyDetails.SurveyEndDate,
        surveyLink: `https://app.evaomax.com/`,
      };
    } else if (ApprovalType === "User Survey Group Approval") {
      emailTemplateContext = {
        userEmail: userEmail,
        surveyGroupName: surveyDetails.SurveyProductName,
        surveyGroupDescription: surveyDetails.SurveyProductDesc,
        surveyLink: `https://app.evaomax.com/`,
      };
    }

    const surveyMailOptions = {
      from: "mallalokesh23@gmail.com",
      to: userEmail,
      subject: `New Survey Assigned: ${emailTemplateContext.surveyName || emailTemplateContext.surveyGroupName}`,
      html: ` <!DOCTYPE html>
              <html lang="en">
                <head>
                  <meta charset="UTF-8">
                  <meta http-equiv="X-UA-Compatible" content="IE=edge">
                  <meta name="viewport" content="width=device-width, initial-scale=1.0">
                  <title>New Survey Assigned</title>
                  <style>
                    body {
                      font-family: Arial, sans-serif;
                      background-color: #f4f4f4;
                      margin: 0;
                      padding: 0;
                      color: #333;
                    }
                    .container {
                      max-width: 600px;
                      margin: 50px auto;
                      background-color: #fff;
                      padding: 20px;
                      border-radius: 8px;
                      box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                    }
                    .header {
                      text-align: center;
                      background-color: #212121;
                      border-bottom: 1px solid #e6e6e6;
                      padding-bottom: 20px;
                      margin-bottom: 20px;
                    }
                    .header img {
                      margin-top: 10px;
                      max-width: 50px;
                      width: 20%;
                      display: inline-block;
                      vertical-align: middle;
                    }
                    .header h1 {
                      font-size: 24px;
                      color: #fff;
                      margin: 0;
                    }
                    .content {
                      text-align: center;
                    }
                    .content h2 {
                      font-size: 20px;
                      color: #333;
                      margin: 0 0 20px;
                    }
                    .survey-info {
                      font-size: 18px;
                      font-weight: bold;
                      color: #337ab7; /* blue color */
                      margin-bottom: 10px;
                    }
                    .highlight {
                      background-color: #ffff99; /* yellow highlight */
                      padding: 2px 4px;
                      border-radius: 4px;
                    }
                    .survey-description {
                      font-size: 16px;
                      color: #666;
                      line-height: 1.6;
                      margin-bottom: 20px;
                    }
                    .btn-link {
                      background-color: #002984;
                      color: #ffffff !important;
                      text-decoration: none;
                      border-radius: 5px;
                      padding: 10px 20px;
                      display: inline-block;
                    }
                    .btn-link:hover {
                      background-color: #001c6c;
                      color: #ffffff !important;
                    }
                    .footer {
                      text-align: center;
                      padding-top: 20px;
                      border-top: 1px solid #e6e6e6;
                      margin-top: 20px;
                      color: #999;
                      font-size: 14px;
                    }
                  </style>
                </head>
                <body>
                  <div class="container">
                    <div class="header">
                      <img src="https://coeindex0storage.blob.core.windows.net/file-uploads/EVA%20LOGO.png" alt="Logo" style="margin-top: 10px;">
                      <h1>New Survey Assigned</h1>
                    </div>
                    <div class="content">
                      <h2>Hello, ${emailTemplateContext.userEmail}</h2>
                      ${
                        ApprovalType === "User Individual Survey Approval"
                          ? `
                            <p class="survey-info">You have been assigned a new survey:</p>
                            <p class="survey-description"><span class="highlight">${emailTemplateContext.surveyName}</span></p>
                            <p class="survey-description">${emailTemplateContext.surveyDescription}</p>
                            <p class="survey-description">This survey is available from <span class="highlight">${emailTemplateContext.startTime}</span> to <span class="highlight">${emailTemplateContext.endTime}</span>.</p>
                          `
                          : `
                            <p class="survey-info">You have been assigned a new survey group:</p>
                            <p class="survey-description"><span class="highlight">${emailTemplateContext.surveyGroupName}</span></p>
                            <p class="survey-description">${emailTemplateContext.surveyGroupDescription}</p>
                          `
                      }
                      <p>Click the link below to access the survey:</p>
                      <a href="${emailTemplateContext.surveyLink}" class="btn-link">Take the Survey</a>
                    </div>
                    <div class="footer">
                      <p>If you have any questions or need assistance, feel free to contact us at EVA@CloudOMax.com.</p>
                      <p>Thank you!</p>
                    </div>
                  </div>
                </body>
              </html>
            `,
            context: emailTemplateContext,
            };

            await sendInvitationEmail(surveyMailOptions, emailTemplateContext);

    return res
      .status(200)
      .json({ success: true, message: "Record inserted And Email sent successfully" });
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({ error: "Internal server error" });
  }
};

const getSurveyDetails = async (SurveyID, SurveyProductID, ApprovalType) => {
  const pool = await getConnection();
  let surveyDetails;
  if (ApprovalType === "User Individual Survey Approval") {
    const surveyQuery = `
      SELECT 
        [SurveyName], 
        [SurveyDesc], 
        [SurveyStartDate], 
        [SurveyEndDate]
      FROM 
        [dbo].[Survey]
      WHERE 
        [SurveyID] = @SurveyID
    `;
    const surveyResult = await pool.request()
      .input("SurveyID", SurveyID)
      .query(surveyQuery);
    surveyDetails = surveyResult.recordset[0];
  } else if (ApprovalType === "User Survey Group Approval") {
    const surveyGroupQuery = `
      SELECT 
        [SurveyProductName], 
        [SurveyProductDesc]
      FROM 
        [dbo].[SurveyGroup]
      WHERE 
        [SurveyProductID] = @SurveyProductID
    `;
    const surveyGroupResult = await pool.request()
      .input("SurveyProductID", SurveyProductID)
      .query(surveyGroupQuery);
    surveyDetails = surveyGroupResult.recordset[0];
  }
  return surveyDetails;
};

export const getAllSurveyByCompanyId = async (req, res) => {
  try {
    const { CompanyID } = req.query;

    if (!CompanyID || isNaN(Number(CompanyID))) {
      return res.status(400).json({
        success: false,
        message: "CompanyID must be a valid number",
      });
    }

    const pool = await getConnection();
    const result = await pool.request().input("CompanyID", Number(CompanyID))
      .query(`SELECT * FROM [dbo].[Approval] WHERE [CompanyID] = @CompanyID AND [ApprovalType] IN 
        ('Company Survey Group Approval', 'Company Individual Survey Approval')`);

    return res.status(200).json({
      success: true,
      data: result.recordset,
      message: "Surveys retrieved successfully",
    });
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({
      success: false,
      message: "Internal server error",
    });
  }
};

export const getAllSurveyByUserId = async (req, res) => {
  try {
    const { UserID } = req.query;

    if (!UserID || isNaN(Number(UserID))) {
      return res.status(400).json({
        success: false,
        message: "UserID must be a valid number",
      });
    }

    const pool = await getConnection();
    const result = await pool.request().input("UserID", Number(UserID))
      .query(`SELECT * FROM [dbo].[Approval] WHERE [UserID] = @UserID AND [ApprovalType] IN 
        ('User Individual Survey Approval','User Survey Group Approval')`);

    return res.status(200).json({
      success: true,
      data: result.recordset,
      message: "Surveys retrieved successfully",
    });
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({
      success: false,
      message: "Internal server error",
    });
  }
};

export const getSurveyStatus = async (req, res) => {
  try {
    const { UserID, SurveyID } = req.query;

    if (!UserID || !SurveyID || isNaN(Number(UserID)) || isNaN(Number(SurveyID))) {
      return res.status(400).json({
        success: false,
        message: "userId and surveyId must be valid numbers",
      });
    }

    const pool = await getConnection();

    const trackerResponse = await pool.request()
      .input('UserID', Number(UserID))
      .input('SurveyID', Number(SurveyID))
      .query(`
        SELECT [TrackerID], [UserID], [SurveyID], [LastAnswerQuestionID], [IsCompleted], [CompletionDate], [LastUpdatedDate]
        FROM [dbo].[Tracker]
        WHERE UserID = @UserID AND SurveyID = @SurveyID
      `);

    const surveyStatus = trackerResponse.recordset[0];

    const evalResponse = await pool.request()
      .input('UserID', Number(UserID))
      .input('SurveyID', Number(SurveyID))
      .query(`
        SELECT COUNT(*) AS ResponseCount
        FROM [dbo].[FactEval]
        WHERE UserID = @UserID AND SurveyID = @SurveyID
      `);

    const responseCount = evalResponse.recordset[0].ResponseCount;

    let status = 'Not Started';
    if (surveyStatus) {
      if (surveyStatus.IsCompleted) {
        status = 'Completed';
      } else if (responseCount > 0) {
        status = 'In Progress';
      }
    }

    return res.status(200).json({
      success: true,
      status: status,
    });
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({
      success: false,
      message: "Internal server error",
    });
  }
};




export const deleteAssignedSurveyFromUser = async (req, res) => {
  try {
    const { ApprovalID ,surveyName} = req.body;
    if (isNaN(ApprovalID)) {
      return res.status(400).json({
        success: false,
        message: "ApprovalID must be a number",
      });
    }
    // Find the UserID based on the ApprovalID
    const pool = await getConnection();
    const userResult = await pool.request().input("ApprovalID", ApprovalID).query(`
        SELECT [UserID] FROM [dbo].[Approval] WHERE [ApprovalID] = @ApprovalID
      `);
    if (userResult.recordset.length === 0) {
      return res.status(404).json({
        success: false,
        message: "Record not found or already deleted",
      });
    }

    const UserID = userResult.recordset[0].UserID;

    const Delete = await pool.request().input("ApprovalID", ApprovalID).query(`
        DELETE FROM [dbo].[Approval] WHERE [ApprovalID] = @ApprovalID
      `);
    if (Delete.rowsAffected[0] === 0) {
      return res.status(404).json({
        success: false,
        message: "Record not found or already deleted",
      });
    }

    // Delete the file from Azure Blob Storage
    const containerName = "report";
    const blobPrefix = `user/${UserID}/`;
    await deleteFileFromBlob(containerName, blobPrefix, surveyName);

    return res.status(200).json({
      success: true,
      message: "Record Deleted Successfully",
    });
  } catch (error) {
    console.log("Error", error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
};

export const GetUserByID = async (req, res) => {
  try {
    const { UserID } = req.query;
    if (isNaN(UserID)) {
      return res.status(400).json({
        success: false,
        message: "UserID must be a number",
      });
    }
    const pool = await getConnection();
    const result = await pool.request().input("UserID", UserID).query(`
        SELECT * FROM [dbo].[User] WHERE [UserID] = @UserID
      `);
    return res.status(200).json({
      success: true,
      data: result.recordset,
    });
  } catch (error) {
    console.log("Error", error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
};

export const updateUserById = async (req, res) => {
  try {
    const {
      UserID,
      UserFirstName,
      UserLastName,
      UserEmail,
      UserPhoneNumber,
      UserAge,
      UserGender,
      UserEducationLevel,
      UserOccupation,
      UserIncomeRange,
      UserCountry,
      UserState,
      UserCity,
      UserLinkedInProfileLink,
      UserFacebookProfileLink,
      UserInstagramProfileLink,
      UserTwitterProfileLink,
      UserCurrentCompanyLink,
      UserPastCompaniesLink,
      UserCompanyID,
      AuthId,
      UserCommunicationEmail,
      UserPhone,
      UserStreet1,
      UserStreet2,
      UserZip,
      UserJobTitle,
      UserCompanyEmail,
      UserCompanyName,
      UserJobIndustry,
      UserProfessionalExpertise,
      UserProfessionalSkills,
      UserCommunityInvolvement,
      UserUniversityAttended,
      UserGraduationYear,
      UserLeadershipProgramGraduatedFrom,
      UserIsToBeRegistered,
      UserIsAlumni,
      UserIsInCurrentClass,
      UserIsChamberMember,
      UserLeadershipBatchYear,
      UserInterestedInLeadershipTopics,
      UserSeekingMentor,
      UserWillingToBeMentor,
      UserMentorshipInvolvementList,
      UserWillingToBeGuestSpeaker,
      UserGuestSpeakerTopicPreference,
      UserIsActive,
      UserMemberSinceDate,
      UserLastUpdated,
      UserIsAdmin,
    } = req.body;

    // Validate required fields
    if (isNaN(UserID)) {
      return res.status(400).json({
        success: false,
        message: "UserID must be a number",
      });
    }

    if (!UserEmail) {
      return res.status(400).json({
        success: false,
        message: "UserEmail is required",
      });
    }

    // Construct the SQL query dynamically
    let query = "UPDATE [dbo].[User] SET ";
    const fields = [
      { name: "UserFirstName", value: UserFirstName },
      { name: "UserLastName", value: UserLastName },
      { name: "UserEmail", value: UserEmail },
      { name: "UserPhoneNumber", value: UserPhoneNumber },
      { name: "UserAge", value: UserAge },
      { name: "UserGender", value: UserGender },
      { name: "UserEducationLevel", value: UserEducationLevel },
      { name: "UserOccupation", value: UserOccupation },
      { name: "UserIncomeRange", value: UserIncomeRange },
      { name: "UserCountry", value: UserCountry },
      { name: "UserState", value: UserState },
      { name: "UserCity", value: UserCity },
      { name: "UserLinkedInProfileLink", value: UserLinkedInProfileLink },
      { name: "UserFacebookProfileLink", value: UserFacebookProfileLink },
      { name: "UserInstagramProfileLink", value: UserInstagramProfileLink },
      { name: "UserTwitterProfileLink", value: UserTwitterProfileLink },
      { name: "UserCurrentCompanyLink", value: UserCurrentCompanyLink },
      { name: "UserPastCompaniesLink", value: UserPastCompaniesLink },
      { name: "UserCompanyID", value: UserCompanyID },
      { name: "AuthId", value: AuthId },
      { name: "UserCommunicationEmail", value: UserCommunicationEmail },
      { name: "UserPhone", value: UserPhone },
      { name: "UserStreet1", value: UserStreet1 },
      { name: "UserStreet2", value: UserStreet2 },
      { name: "UserZip", value: UserZip },
      { name: "UserJobTitle", value: UserJobTitle },
      { name: "UserCompanyEmail", value: UserCompanyEmail },
      { name: "UserCompanyName", value: UserCompanyName },
      { name: "UserJobIndustry", value: UserJobIndustry },
      { name: "UserProfessionalExpertise", value: UserProfessionalExpertise },
      { name: "UserProfessionalSkills", value: UserProfessionalSkills },
      { name: "UserCommunityInvolvement", value: UserCommunityInvolvement },
      { name: "UserUniversityAttended", value: UserUniversityAttended },
      { name: "UserGraduationYear", value: UserGraduationYear },
      {
        name: "UserLeadershipProgramGraduatedFrom",
        value: UserLeadershipProgramGraduatedFrom,
      },
      { name: "UserIsToBeRegistered", value: UserIsToBeRegistered },
      { name: "UserIsAlumni", value: UserIsAlumni },
      { name: "UserIsInCurrentClass", value: UserIsInCurrentClass },
      { name: "UserIsChamberMember", value: UserIsChamberMember },
      { name: "UserLeadershipBatchYear", value: UserLeadershipBatchYear },
      {
        name: "UserInterestedInLeadershipTopics",
        value: UserInterestedInLeadershipTopics,
      },
      { name: "UserSeekingMentor", value: UserSeekingMentor },
      { name: "UserWillingToBeMentor", value: UserWillingToBeMentor },
      {
        name: "UserMentorshipInvolvementList",
        value: UserMentorshipInvolvementList,
      },
      {
        name: "UserWillingToBeGuestSpeaker",
        value: UserWillingToBeGuestSpeaker,
      },
      {
        name: "UserGuestSpeakerTopicPreference",
        value: UserGuestSpeakerTopicPreference,
      },
      { name: "UserIsActive", value: UserIsActive },
      { name: "UserMemberSinceDate", value: UserMemberSinceDate },
      { name: "UserLastUpdated", value: UserLastUpdated },
      { name: "UserIsAdmin", value: UserIsAdmin },
    ];

    const nonNullFields = fields.filter((field) => field.value !== undefined);
    const inputParameters = {};

    nonNullFields.forEach((field, index) => {
      query += `${field.name} = @${field.name}`;
      if (index < nonNullFields.length - 1) {
        query += ", ";
      }
      inputParameters[field.name] = field.value;
    });

    query += " WHERE UserID = @UserID";

    const pool = await getConnection();
    const request = pool.request();

    Object.entries(inputParameters).forEach(([key, value]) => {
      request.input(key, value);
    });

    request.input("UserID", UserID);

    const update = await request.query(query);

    if (update.rowsAffected[0] === 0) {
      return res.status(404).json({
        success: false,
        message: "Record not found or already deleted",
      });
    }

    res.status(200).json({
      success: true,
      message: "User updated successfully",
    });
  } catch (error) {
    res.status(500).json({
      success: false,
      message: "An error occurred while updating the user",
      error: error.message,
    });
  }
};


export const uploadPdf = async (req, res) => {
  try {
    const containerName = "report";
    const userId = req.body.userId;
    const companyId = req.body.companyId; 
    const files = req.files;
     
      // Get the userEmail associated with the UserID
      const pool = await getConnection();
      const getUserEmailQuery = `
      SELECT UserEmail FROM [dbo].[User] WHERE [UserID] = @UserID
    `;
    const userEmailResult = await pool
      .request()
      .input("UserID", userId)
      .query(getUserEmailQuery);

    const userEmail = userEmailResult.recordset[0].UserEmail;

    if (!files || files.length === 0) {
      return res
        .status(400)
        .json({ success: false, message: "No files uploaded" });
    }

    const blobUrls = [];

    for (const file of files) {
      if (file.mimetype !== "application/pdf") {
        return res
          .status(400)
          .json({ success: false, message: "Only PDF files are allowed" });
      }

      let folderType;
      let folderId;

      if (userId) {
        folderType = "user";
        folderId = userId;
      } else if (companyId) {
        folderType = "company";
        folderId = companyId;
      } else {
        return res.status(400).json({
          success: false,
          message: "Either userId or companyId must be provided",
        });
      }

      const blobName = `${folderType}/${folderId}/${file.originalname}`;
      const blobUrl = await uploadFileToBlob(
        containerName,
        blobName,
        file.buffer,
        file.mimetype
      );
      blobUrls.push(blobUrl);
    }
    
    const assessmentReportName = files[0].originalname;

    const emailTemplateContext = {
      userEmail: userEmail,
      assessmentReportName: assessmentReportName,
      downloadLink : blobUrls[0]
    };

    const mailOptions = {
      from: "Bhargav.Konathala@cloudomaxcorp.com",
      to: userEmail,
      subject: `Assessment Report Generated: ${assessmentReportName}`,
      html: `   <!DOCTYPE html>
                <html lang="en">
                  <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Assessment Report Generated</title>
                    <style>
                      body {
                        font-family: Arial, sans-serif;
                        background-color: #f4f4f4;
                        margin: 0;
                        padding: 0;
                        color: #333;
                      }
                      .container {
                        max-width: 600px;
                        margin: 50px auto;
                        background-color: #fff;
                        padding: 20px;
                        border-radius: 8px;
                        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                      }
                      .header {
                        text-align: center;
                        background-color: #212121;
                        border-bottom: 1px solid #e6e6e6;
                        padding-bottom: 20px;
                        margin-bottom: 20px;
                      }
                      .header img {
                        margin-top: 10px;
                        max-width: 50px;
                        width: 20%;
                        display: inline-block;
                        vertical-align: middle;
                      }
                      .header h1 {
                        font-size: 24px;
                        color: #fff;
                        margin: 0;
                      }
                      .content {
                        text-align: center;
                      }
                      .content h2 {
                        font-size: 20px;
                        color: #333;
                        margin: 0 0 20px;
                      }
                      .assessment-report-info {
                        font-size: 18px;
                        font-weight: bold;
                        color: #337ab7; /* blue color */
                        margin-bottom: 10px;
                      }
                      .assessment-report-description {
                        font-size: 16px;
                        color: #666;
                        line-height: 1.6;
                        margin-bottom: 20px;
                      }
                      .btn-link {
                        background-color: #002984;
                        color: #ffffff !important;
                        text-decoration: none;
                        border-radius: 5px;
                        padding: 10px 20px;
                        display: inline-block;
                      }
                      .btn-link:hover {
                        background-color: #001c6c;
                        color: #ffffff !important;
                      }
                      .footer {
                        text-align: center;
                        padding-top: 20px;
                        border-top: 1px solid #e6e6e6;
                        margin-top: 20px;
                        color: #999;
                        font-size: 14px;
                      }
                    </style>
                  </head>
                  <body>
                    <div class="container">
                      <div class="header">
                        <img src="https://coeindex0storage.blob.core.windows.net/file-uploads/EVA%20LOGO.png" alt="Logo" style="margin-top: 10px;">
                        <h1>Assessment Report Generated</h1>
                      </div>
                      <div class="content">
                        <h2>Hello, ${emailTemplateContext.userEmail}</h2>
                        <p class="assessment-report-info">Your assessment report is generated:</p>
                        <p class="assessment-report-description">${emailTemplateContext.assessmentReportName}</p>
                        <p>You can check it in the app at <a href="https://app.evamax.com">app.evamax.com</a> or download it here:</p>
                        <p><a href="${emailTemplateContext.downloadLink}" class="btn-link">Download Report</a></p>
                      </div>
                      <div class="footer">
                        <p>If you have any questions or need assistance, feel free to contact us at EVA@CloudOMax.com.</p>
                        <p>Thank you!</p>
                      </div>
                    </div>
                  </body>
                </html>
              `,
              context: emailTemplateContext,
            };

    await sendInvitationEmail(mailOptions,emailTemplateContext);

    return res.status(200).json({ success: true, blobUrls });
  } catch (error) {
    console.error("Error uploading files:", error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

export const uploadProfileImage = async (req, res) => {
  const accountName = process.env.AZURE_STORAGE_ACCOUNT_NAME;
  const accountKey = process.env.AZURE_STORAGE_ACCOUNT_KEY;
 
  if (!accountName || !accountKey) {
    throw new Error('Azure Storage account name or account key is missing in .env file');
  }

  const sharedKeyCredential = new StorageSharedKeyCredential(accountName, accountKey);
  const blobServiceClient = new BlobServiceClient(`https://${accountName}.blob.core.windows.net`, sharedKeyCredential);
  try {
    const containerName = "profile-images";
    const userId = req.body.userId;
    const file = req.file;

    console.log('File:', file);

    if (!file) {
      return res.status(400).json({ success: false, message: "No file uploaded" });
    }

    if (file.mimetype !== "image/jpeg" && file.mimetype !== "image/png") {
      return res.status(400).json({ success: false, message: "Only JPEG and PNG images are allowed" });
    }

    if (!userId) {
      return res.status(400).json({ success: false, message: "userId must be provided" });
    }


    const containerClient = blobServiceClient.getContainerClient(containerName);
    

    const blobIterator = containerClient.listBlobsFlat({ prefix: `user/${userId}/` });
    

    for await (const blob of blobIterator) {
      await containerClient.getBlockBlobClient(blob.name).deleteIfExists();
    }


    const blobName = `user/${userId}/${file.originalname}`;
    const blobUrl = await uploadFileToBlob(containerName, blobName, file.buffer, file.mimetype);

    return res.status(200).json({ success: true, imageUrl: blobUrl });
  } catch (error) {
    console.error("Error uploading file:", error);
    return res.status(500).json({ success: false, message: "Internal server error" });
  }
};


export const getProfileImageUrl = async (req, res) => {
  try {
    const containerName = "profile-images";
    const userId = req.query.userId;

    if (!userId) {
      return res.status(400).json({ success: false, message: "userId must be provided" });
    }

    const blobUrl = await getBlobUrl(containerName, `user/${userId}/`);

    if (!blobUrl) {
      return res.status(404).json({ success: false, message: "Profile image not found" });
    }

    return res.status(200).json({ success: true, imageUrl: blobUrl });
  } catch (error) {
    console.error("Error getting profile image URL:", error);
    return res.status(500).json({ success: false, message: "Internal server error" });
  }
};

// export const uploadFileToBlob = async (containerName, blobName, fileBuffer, contentType) => {
//   const containerClient = blobServiceClient.getContainerClient(containerName);
//   const blockBlobClient = containerClient.getBlockBlobClient(blobName);

//   await blockBlobClient.uploadData(fileBuffer, {
//     blobHTTPHeaders: { blobContentType: contentType },
//   });

//   return blockBlobClient.url;
// };

// export const uploadPdf = async (req, res) => {
//   try {
//     const containerName = 'report';
//     const userId = req.body.userId;
//     const companyId = req.body.companyId;
//     const files = req.files;

//     if (!files || files.length === 0) {
//       return res.status(400).json({ success: false, message: 'No files uploaded' });
//     }

//     const blobUrls = [];

//     for (const file of files) {
//       if (file.mimetype !== 'application/pdf') {
//         return res.status(400).json({ success: false, message: 'Only PDF files are allowed' });
//       }

//       let folderType;
//       let folderId;

//       if (userId) {
//         folderType = 'user';
//         folderId = userId;
//       } else if (companyId) {
//         folderType = 'company';
//         folderId = companyId;
//       } else {
//         return res.status(400).json({ success: false, message: 'Either userId or companyId must be provided' });
//       }

//       const blobName = `${folderType}/${folderId}/${file.originalname}`;
//       const blobUrl = await uploadFileToBlob(containerName, blobName, file.buffer, file.mimetype);
//       blobUrls.push(blobUrl);
//     }

//     return res.status(200).json({ success: true, blobUrls });
//   } catch (error) {
//     console.error('Error uploading files:', error);
//     return res.status(500).json({ success: false, message: 'Internal server error' });
//   }
// };

// export const getPdfsByUserId = async (req, res) => {
//   try {
//     const { userId } = req.query;
//     const containerName = 'report';

//     if (!userId) {
//       return res.status(400).json({ success: false, message: 'User ID is required' });
//     }

//     const containerClient = blobServiceClient.getContainerClient(containerName);
//     const prefix = `user/${userId}/`;
//     const blobUrls = [];

//     for await (const blob of containerClient.listBlobsFlat({ prefix })) {
//       const blobClient = containerClient.getBlobClient(blob.name);
//       blobUrls.push(blobClient.url);
//     }

//     return res.status(200).json({ success: true, blobUrls });
//   } catch (error) {
//     console.error('Error fetching PDFs:', error);
//     return res.status(500).json({ success: false, message: 'Internal server error' });
//   }
// };

export const getPdfsByUserId = async (req, res) => {
  const accountName = process.env.AZURE_STORAGE_ACCOUNT_NAME;
  const accountKey = process.env.AZURE_STORAGE_ACCOUNT_KEY;
 
  if (!accountName || !accountKey) {
    throw new Error('Azure Storage account name or account key is missing in .env file');
  }

  const sharedKeyCredential = new StorageSharedKeyCredential(accountName, accountKey);
  const blobServiceClient = new BlobServiceClient(`https://${accountName}.blob.core.windows.net`, sharedKeyCredential);

  try {
    const { UserID } = req.query;
    const containerName = "report";

    if (!UserID) {
      return res
        .status(400)
        .json({ success: false, message: "User ID is required" });
    }

    const containerClient = blobServiceClient.getContainerClient(containerName);
    const prefix = `user/${UserID}/`;
    const pdfNames = [];
    let userExists = false;

    for await (const blob of containerClient.listBlobsFlat({ prefix })) {
      userExists = true;
      let blobName = blob.name.split("/").pop(); // Extract the file name from the blob path
      blobName = blobName.replace(".pdf", ""); // Remove the .pdf extension
      pdfNames.push(blobName);
    }

    if (!userExists) {
      return res.status(404).json({
        success: false,
        message: "User ID does not exist or has no associated PDFs",
      });
    }

    return res.status(200).json({ success: true, pdfNames });
  } catch (error) {
    console.error("Error fetching PDFs:", error);
    return res
      .status(500)
      .json({ success: false, message: "Internal server error" });
  }
};

export const deleteTrackerId = async (req, res) => {
  try {
    const { TrackerID } = req.body;

    if (!TrackerID) {
      return res.status(400).json({ error: "TrackerID is required" });
    }

    const pool = await getConnection();
    const transaction = pool.transaction();
    await transaction.begin();

    try {
      await transaction.request().input("TrackerID", TrackerID).query(`
          DELETE FROM [dbo].[FactEval] WHERE [TrackerID] = @TrackerID
        `);

      // Delete from Tracker table
      const DeleteTracker = await transaction
        .request()
        .input("TrackerID", TrackerID).query(`
          DELETE FROM [dbo].[Tracker] WHERE [TrackerID] = @TrackerID
        `);

      await transaction.commit();

      if (DeleteTracker.rowsAffected[0] === 0) {
        return res.status(404).json({
          success: false,
          message: "TrackerID not found or already deleted",
        });
      }

      return res.status(200).json({
        success: true,
        message: `TrackerID: ${TrackerID} Deleted Successfully`,
      });
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  } catch (error) {
    console.log("Error", error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
};

// export const createConsultantAdmin = async (req, res) => {
//   try {
//     const { UserEmail } = req.body;
//     const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
//     if (!emailRegex.test(UserEmail)) {
//       return res.status(400).json({
//         success: false,
//         message: 'Invalid email format',
//       });
//     }

//     const pool = await getConnection();
//     const result = await pool.request()
//       .input('UserEmail', UserEmail)
//       .query(`
//         SELECT UserId FROM [dbo].[User] WHERE [UserEmail] = @UserEmail`);

//     if (result.recordset.length > 0) {
//       const UserId = result.recordset[0].UserId;
//       return res.status(200).json({
//         success: true,
//         UserId: UserId,
//       });
//     } else {
//       return res.status(400).json({
//         success: false,
//         message: 'User not found',
//       });
//     }
//   } catch (error) {
//     return res.status(500).json({
//       success: false,
//       message: 'Internal server error',
//       error: error.message,
//     });
//   }
// };

export const createConsultantAdmin = async (req, res) => {
  try {
    const { UserEmail ,TemporaryPassword} = req.body;
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(UserEmail)) {
      return res.status(400).json({
        success: false,
        message: "Invalid email format",
      });
    }

    const pool = await getConnection();
    const userResult = await pool.request().input("UserEmail", UserEmail)
      .query(`
        SELECT UserId FROM [dbo].[User] WHERE [UserEmail] = @UserEmail`);

    if (userResult.recordset.length > 0) {
      const UserId = userResult.recordset[0].UserId;

      const approvalResult = await pool
        .request()
        .input("UserId", UserId)
        .input("RoleId", 4).query(`
          UPDATE [dbo].[Approval]
          SET [RoleId] = @RoleId , [IsApproved] = '1'
          WHERE [UserId] = @UserId AND [ApprovalType] = 'User Role Approval' OR [ApprovalType] = 'User Signup Approval'`);

          const resetPasswordLink = await generatePasswordResetTicket(UserEmail);

          const emailTemplateContext = {
            email: UserEmail,
            password: TemporaryPassword,
            resetPasswordLink: resetPasswordLink
          };
      
          const mailOptions = {
            from: "mallalokesh23@gmail.com",
            to: UserEmail,
            subject: `Welcome to EVA`,
            html: `<!DOCTYPE html>
                    <html lang="en">
                    <head>
                      <meta charset="UTF-8">
                      <meta http-equiv="X-UA-Compatible" content="IE=edge">
                      <meta name="viewport" content="width=device-width, initial-scale=1.0">
                      <title>Welcome to EVA</title>
                      <style>
                        body {
                          font-family: Arial, sans-serif;
                          background-color: #f4f4f4;
                          margin: 0;
                          padding: 0;
                          color: #333;
                        }
                        .container {
                          max-width: 600px;
                          margin: 50px auto;
                          background-color: #fff;
                          padding: 20px;
                          border-radius: 8px;
                          box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                        }
                        .header {
                          text-align: center;
                          background-color: #212121;
                          border-bottom: 1px solid #e6e6e6;
                          padding-bottom: 20px;
                          margin-bottom: 20px;
                        }
                        .header img {
                          margin-top: 10px;
                          max-width: 50px;
                          width: 20%;
                          display: inline-block;
                          vertical-align: middle;
                        }
                        .header h1 {
                          font-size: 24px;
                          color: #fff;
                          margin: 0;
                        }
                        .content {
                          text-align: center;
                        }
                        .content h2 {
                          font-size: 20px;
                          color: #333;
                          margin: 0 0 20px;
                        }
                        .content p {
                          font-size: 16px;
                          color: #666;
                          line-height: 1.6;
                        }
                        .btn-link {
                          background-color: #002984;
                          color: #ffffff !important;
                          text-decoration: none;
                          border-radius: 5px;
                          padding: 10px 20px;
                          display: inline-block;
                        }

                        .btn-link:hover {
                          background-color: #001c6c;
                          color: #ffffff !important;
                        }

                        .btn-reset {
                          color: #002984;
                          text-decoration: none;
                          font-weight: bold;
                        }
                        .footer {
                          text-align: center;
                          padding-top: 20px;
                          border-top: 1px solid #e6e6e6;
                          margin-top: 20px;
                          color: #999;
                          font-size: 14px;
                        }
                      </style>
                    </head>
                    <body>
                      <div class="container">
                        <div class="header">
                          <img src="https://coeindex0storage.blob.core.windows.net/file-uploads/EVA%20LOGO.png" alt="Logo" style="margin-top: 10px;">
                          <h1>Welcome to EVA</h1>
                        </div>
                        <div class="content">
                          <h2>Hello, ${emailTemplateContext.email}</h2>
                          <p>Welcome to EVA - Evaluation Virtual Assistance. We're excited to have you on board!</p>
                          <p>Please find your login details below:</p>
                          <p><strong>Email:</strong> ${emailTemplateContext.email}</p>
                          <p><strong>Temporary Password:</strong> ${emailTemplateContext.password}</p>
                          <p>For your security, please reset your password using the link below:</p>
                          <a href="${emailTemplateContext.resetPasswordLink}" class="btn-reset">Reset Password</a>
                          <p>Once your password is reset, you can log in to our website using the following link:</p>
                          <a href="https://app.evaomax.com/" class="btn-link">Log In to EVA</a>
                        </div>
                        <div class="footer">
                          <p>If you have any questions or need assistance, feel free to contact us at EVA@CloudOMax.com.</p>
                          <p>Thank you!</p>
                        </div>
                      </div>
                    </body>
                    </html>`,
            context: emailTemplateContext,
          };
    
          await sendInvitationEmail(mailOptions,emailTemplateContext);
    
      return res.status(200).json({
        success: true,
        UserId: UserId,
        message: "Role updated & mail sent successfully",
      });
    } else {
      return res.status(404).json({
        success: false,
        message: "User not found",
      });
    }
  } catch (error) {
    return res.status(500).json({
      success: false,
      message: "Internal server error",
      error: error.message,
    });
  }
};

export const createCompanyAdmin = async (req, res) => {
  try {
    const { UserEmail ,TemporaryPassword} = req.body;
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(UserEmail)) {
      return res.status(400).json({
        success: false,
        message: "Invalid email format",
      });
    }

    const pool = await getConnection();
    const userResult = await pool.request().input("UserEmail", UserEmail)
      .query(`
        SELECT UserId FROM [dbo].[User] WHERE [UserEmail] = @UserEmail`);

    if (userResult.recordset.length > 0) {
      const UserId = userResult.recordset[0].UserId;

      await pool.request().input("UserId", UserId).query(`
            UPDATE [dbo].[Approval]
            SET [IsApproved] = '1' 
            WHERE [UserId] = @UserId AND ([ApprovalType] = 'User Role Approval' OR [ApprovalType] = 'User Signup Approval')`);

      const approvalResult = await pool
        .request()
        .input("UserId", UserId)
        .input("RoleId", 3).query(`
          UPDATE [dbo].[Approval]
          SET [RoleId] = @RoleId
          WHERE [UserId] = @UserId AND [ApprovalType] = 'User Role Approval'`);

          const resetPasswordLink = await generatePasswordResetTicket(UserEmail);

          const emailTemplateContext = {
            email: UserEmail,
            password: TemporaryPassword,
            resetPasswordLink: resetPasswordLink
          };
      
          const mailOptions = {
            from: "Bhargav.Konathala@cloudomaxcorp.com",
            to: UserEmail,
            subject: `Welcome to EVA`,
            html: `<!DOCTYPE html>
                    <html lang="en">
                    <head>
                      <meta charset="UTF-8">
                      <meta http-equiv="X-UA-Compatible" content="IE=edge">
                      <meta name="viewport" content="width=device-width, initial-scale=1.0">
                      <title>Welcome to EVA</title>
                      <style>
                        body {
                          font-family: Arial, sans-serif;
                          background-color: #f4f4f4;
                          margin: 0;
                          padding: 0;
                          color: #333;
                        }
                        .container {
                          max-width: 600px;
                          margin: 50px auto;
                          background-color: #fff;
                          padding: 20px;
                          border-radius: 8px;
                          box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                        }
                        .header {
                          text-align: center;
                          background-color: #212121;
                          border-bottom: 1px solid #e6e6e6;
                          padding-bottom: 20px;
                          margin-bottom: 20px;
                        }
                        .header img {
                          margin-top: 10px;
                          max-width: 50px;
                          width: 20%;
                          display: inline-block;
                          vertical-align: middle;
                        }
                        .header h1 {
                          font-size: 24px;
                          color: #fff;
                          margin: 0;
                        }
                        .content {
                          text-align: center;
                        }
                        .content h2 {
                          font-size: 20px;
                          color: #333;
                          margin: 0 0 20px;
                        }
                        .content p {
                          font-size: 16px;
                          color: #666;
                          line-height: 1.6;
                        }
                        .btn-link {
                          background-color: #002984;
                          color: #ffffff !important;
                          text-decoration: none;
                          border-radius: 5px;
                          padding: 10px 20px;
                          display: inline-block;
                        }

                        .btn-link:hover {
                          background-color: #001c6c;
                          color: #ffffff !important;
                        }
                          .btn-reset {
                          color: #002984;
                          text-decoration: none;
                          font-weight: bold;
                        }
                        .footer {
                          text-align: center;
                          padding-top: 20px;
                          border-top: 1px solid #e6e6e6;
                          margin-top: 20px;
                          color: #999;
                          font-size: 14px;
                        }
                      </style>
                    </head>
                    <body>
                      <div class="container">
                        <div class="header">
                          <img src="https://coeindex0storage.blob.core.windows.net/file-uploads/EVA%20LOGO.png" alt="Logo" style="margin-top: 10px;">
                          <h1>Welcome to EVA</h1>
                        </div>
                        <div class="content">
                          <h2>Hello, ${emailTemplateContext.email}</h2>
                          <p>Welcome to EVA - Evaluation Virtual Assistance. We're excited to have you on board!</p>
                          <p>Please find your login details below:</p>
                          <p><strong>Email:</strong> ${emailTemplateContext.email}</p>
                          <p><strong>Temporary Password:</strong> ${emailTemplateContext.password}</p>
                          <p>For your security, please reset your password using the link below:</p>
                          <a href="${emailTemplateContext.resetPasswordLink}" class="btn-reset">Reset Password</a>
                          <p>Once your password is reset, you can log in to our website using the following link:</p>
                          <a href="https://app.evaomax.com/" class="btn-link">Log In to EVA</a>
                        </div>
                        <div class="footer">
                          <p>If you have any questions or need assistance, feel free to contact us at EVA@CloudOMax.com.</p>
                          <p>Thank you!</p>
                        </div>
                      </div>
                    </body>
                    </html>`,
            context: emailTemplateContext,
          };
    
          await sendInvitationEmail(mailOptions,emailTemplateContext);
    

      return res.status(200).json({
        success: true,
        UserId: UserId,
        message: "Role updated & mail sent successfully",
      });
    } else {
      return res.status(404).json({
        success: false,
        message: "User not found",
      });
    }
  } catch (error) {
    return res.status(500).json({
      success: false,
      message: "Internal server error",
      error: error.message,
    });
  }
};

// export const CreateCompanyUser = async (req, res) => {
//   try {
//     const { UserEmail, UserCompanyName } = req.body;
//     const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

//     if (!emailRegex.test(UserEmail)) {
//       return res.status(400).json({
//         success: false,
//         message: "Invalid email format",
//       });
//     }

//     const pool = await getConnection();
//     const updateResult = await pool
//       .request()
//       .input("UserEmail", UserEmail)
//       .input("UserCompanyName", UserCompanyName).query(`
//         UPDATE [dbo].[User]
//         SET [UserCompanyName] = @UserCompanyName
//         WHERE [UserEmail] = @UserEmail
//       `);

//     if (updateResult.rowsAffected[0] > 0) {
//       await pool
//         .request()
//         .input("UserId", UserId)
//         .query(`
//             UPDATE [dbo].[Approval]
//             SET [IsApproved] = '1'
//             WHERE [UserId] = @UserId AND ([ApprovalType] = 'User Role Approval' OR [ApprovalType] = 'User Signup Approval')
//         `);

//       return res.status(200).json({
//         success: true,
//         message: "Company name updated successfully",
//       });
//     } else {
//       return res.status(404).json({
//         success: false,
//         message: "User not found",
//       });
//     }
//   } catch (error) {
//     console.error("Error updating company name:", error);
//     return res.status(500).json({
//       success: false,
//       message: "Internal server error",
//     });
//   }
// };

export const CreateCompanyUser = async (req, res) => {
  try {
    const { UserEmail, UserCompanyName ,TemporaryPassword} = req.body;
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (!emailRegex.test(UserEmail)) {
      return res.status(400).json({
        success: false,
        message: "Invalid email format",
      });
    }

    const pool = await getConnection();
    const userResult = await pool.request().input("UserEmail", UserEmail)
      .query(`
        SELECT UserId FROM [dbo].[User] WHERE [UserEmail] = @UserEmail`);

    if (userResult.recordset.length > 0) {
      const UserId = userResult.recordset[0].UserId;

      const updateUserCompanyResult = await pool
        .request()
        .input("UserEmail", UserEmail)
        .input("UserCompanyName", UserCompanyName).query(`
          UPDATE [dbo].[User]
          SET [UserCompanyName] = @UserCompanyName  
          WHERE [UserEmail] = @UserEmail
        `);

      await pool.request().input("UserId", UserId).query(`
            UPDATE [dbo].[Approval]
            SET [IsApproved] = '1'
            WHERE [UserId] = @UserId AND ([ApprovalType] = 'User Role Approval' OR [ApprovalType] = 'User Signup Approval')
        `);

        const resetPasswordLink = await generatePasswordResetTicket(UserEmail);

        const emailTemplateContext = {
          email: UserEmail,
          password: TemporaryPassword,
          resetPasswordLink: resetPasswordLink
        };
    
        const mailOptions = {
          from: "Bhargav.Konathala@cloudomaxcorp.com",
          to: UserEmail,
          subject: `Welcome to EVA`,
          html: `<!DOCTYPE html>
                  <html lang="en">
                  <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <title>Welcome to EVA</title>
                    <style>
                      body {
                        font-family: Arial, sans-serif;
                        background-color: #f4f4f4;
                        margin: 0;
                        padding: 0;
                        color: #333;
                      }
                      .container {
                        max-width: 600px;
                        margin: 50px auto;
                        background-color: #fff;
                        padding: 20px;
                        border-radius: 8px;
                        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                      }
                      .header {
                        text-align: center;
                        background-color: #212121;
                        border-bottom: 1px solid #e6e6e6;
                        padding-bottom: 20px;
                        margin-bottom: 20px;
                      }
                      .header img {
                        margin-top: 10px;
                        max-width: 50px;
                        width: 20%;
                        display: inline-block;
                        vertical-align: middle;
                      }
                      .header h1 {
                        font-size: 24px;
                        color: #fff;
                        margin: 0;
                      }
                      .content {
                        text-align: center;
                      }
                      .content h2 {
                        font-size: 20px;
                        color: #333;
                        margin: 0 0 20px;
                      }
                      .content p {
                        font-size: 16px;
                        color: #666;
                        line-height: 1.6;
                      }
                      .btn-link {
                        background-color: #002984;
                        color: #ffffff !important;
                        text-decoration: none;
                        border-radius: 5px;
                        padding: 10px 20px;
                        display: inline-block;
                      }

                      .btn-link:hover {
                        background-color: #001c6c;
                        color: #ffffff !important;
                      }
                        .btn-reset {
                          color: #002984;
                          text-decoration: none;
                          font-weight: bold;
                        }
                      .footer {
                        text-align: center;
                        padding-top: 20px;
                        border-top: 1px solid #e6e6e6;
                        margin-top: 20px;
                        color: #999;
                        font-size: 14px;
                      }
                    </style>
                  </head>
                  <body>
                    <div class="container">
                      <div class="header">
                        <img src="https://coeindex0storage.blob.core.windows.net/file-uploads/EVA%20LOGO.png" alt="Logo" style="margin-top: 10px;">
                        <h1>Welcome to EVA</h1>
                      </div>
                      <div class="content">
                        <h2>Hello, ${emailTemplateContext.email}</h2>
                        <p>Welcome to EVA - Evaluation Virtual Assistance. We're excited to have you on board!</p>
                        <p>Please find your login details below:</p>
                        <p><strong>Email:</strong> ${emailTemplateContext.email}</p>
                        <p><strong>Temporary Password:</strong> ${emailTemplateContext.password}</p>
                        <p>For your security, please reset your password using the link below:</p>
                        <a href="${emailTemplateContext.resetPasswordLink}" class="btn-reset">Reset Password</a>
                        <p>Once your password is reset, you can log in to our website using the following link:</p>
                        <a href="https://app.evaomax.com/" class="btn-link">Log In to EVA</a>
                      </div>
                      <div class="footer">
                        <p>If you have any questions or need assistance, feel free to contact us at EVA@CloudOMax.com.</p>
                        <p>Thank you!</p>
                      </div>
                    </div>
                  </body>
                  </html>`,
          context: emailTemplateContext,
        };
  
        await sendInvitationEmail(mailOptions,emailTemplateContext);
  

      return res.status(200).json({
        success: true,
        UserId: UserId,
        message: "Company name updated and approvals set & mail sent successfully",
      });
    } else {
      return res.status(404).json({
        success: false,
        message: "User not found",
      });
    }
  } catch (error) {
    console.error("Error updating company name and approvals:", error);
    return res.status(500).json({
      success: false,
      message: "Internal server error",
      error: error.message,
    });
  }
};

export const createCompany = async (req, res) => {
  try {
    const {
      CompanyName,
      CompanyType,
      CompanyIndustry,
      CompanyHeadquarters,
      CompanyFoundedYear,
      CompanyRevenue,
      CompanyNumberOfEmployees,
      CompanyMarketSegment,
      CompanyParentCompanyID,
      CompanyGlobalPresence,
      CompanyWebsiteURL,
      CompanyContactInfo,
      CompanyDescription,
      CompanyIsConsultingCompany,
      ConsultingCompanyID,
    } = req.body;

    const pool = await getConnection();
    const result = await pool
      .request()
      .input("CompanyName", CompanyName)
      .input("CompanyType", CompanyType)
      .input("CompanyIndustry", CompanyIndustry)
      .input("CompanyHeadquarters", CompanyHeadquarters)
      .input("CompanyFoundedYear", CompanyFoundedYear)
      .input("CompanyRevenue", CompanyRevenue)
      .input("CompanyNumberOfEmployees", CompanyNumberOfEmployees)
      .input("CompanyMarketSegment", CompanyMarketSegment)
      .input("CompanyParentCompanyID", CompanyParentCompanyID)
      .input("CompanyGlobalPresence", CompanyGlobalPresence)
      .input("CompanyWebsiteURL", CompanyWebsiteURL)
      .input("CompanyContactInfo", CompanyContactInfo)
      .input("CompanyDescription", CompanyDescription)
      .input("CompanyIsConsultingCompany", CompanyIsConsultingCompany)
      .input("ConsultingCompanyID", ConsultingCompanyID).query(`
        INSERT INTO [dbo].[Company] (
          CompanyName,
          CompanyType,
          CompanyIndustry,
          CompanyHeadquarters,
          CompanyFoundedYear,
          CompanyRevenue,
          CompanyNumberOfEmployees,
          CompanyMarketSegment,
          CompanyParentCompanyID,
          CompanyGlobalPresence,
          CompanyWebsiteURL,
          CompanyContactInfo,
          CompanyDescription,
          CompanyIsConsultingCompany,
          ConsultingCompanyID
        ) VALUES (
          @CompanyName,
          @CompanyType,
          @CompanyIndustry,
          @CompanyHeadquarters,
          @CompanyFoundedYear,
          @CompanyRevenue,
          @CompanyNumberOfEmployees,
          @CompanyMarketSegment,
          @CompanyParentCompanyID,
          @CompanyGlobalPresence,
          @CompanyWebsiteURL,
          @CompanyContactInfo,
          @CompanyDescription,
          @CompanyIsConsultingCompany,
          @ConsultingCompanyID
        );
        
        SELECT 
          CompanyID,
          CompanyName,
          CompanyType,
          CompanyIndustry,
          CompanyHeadquarters,
          CompanyFoundedYear,
          CompanyRevenue,
          CompanyNumberOfEmployees,
          CompanyMarketSegment,
          CompanyParentCompanyID,
          CompanyGlobalPresence,
          CompanyWebsiteURL,
          CompanyContactInfo,
          CompanyDescription,
          CompanyIsConsultingCompany,
          ConsultingCompanyID
        FROM [dbo].[Company]
        WHERE CompanyID = SCOPE_IDENTITY();
      `);

    return res.status(200).json({
      success: true,
      message: "Company Created Successfully",
      data: result.recordset[0],
    });
  } catch (error) {
    console.log("Error", error);
    return res.status(500).json({ error: "Internal Server Error" });
  }
};

// export const AddcompanyAdminByConsultantAdmin = async (req, res) => {
//   try {
//     const { UserEmail, ConsultantID } = req.body;
//     const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
//     if (!emailRegex.test(UserEmail)) {
//       return res.status(400).json({
//         success: false,
//         message: "Invalid email format",
//       });
//     }
//     const pool = await getConnection();
//     const userResult = await pool.request().input("UserEmail", UserEmail)
//       .query(`
//           SELECT UserId FROM [dbo].[User] WHERE [UserEmail] = @UserEmail`);

//     if (userResult.recordset.length > 0) {
//       const UserId = userResult.recordset[0].UserId;

//       await pool.request().input("UserId", UserId).query(`
//             UPDATE [dbo].[Approval] 
//             SET [IsApproved] = '1'
//             WHERE [UserId] = @UserId AND ([ApprovalType] = 'User Role Approval' OR [ApprovalType] = 'User Signup Approval')`);

//       const approvalResult = await pool
//         .request()
//         .input("UserId", UserId)
//         .input("RoleId", 3)
//         .input("ConsultantID", ConsultantID).query(`
//             UPDATE [dbo].[Approval]
//             SET [RoleId] = @RoleId, [ConsultantID] = @ConsultantID
//             WHERE [UserId] = @UserId AND [ApprovalType] = 'User Role Approval'`);

//       const emailTemplateContext = {
//         // company: company,
//         email: UserEmail,
//         password: "CoeIndex@123",
//         // name: emp_name,
//         invitationLink: "https://example.com/accept-invitation",
//       };
//       let mailOptions = {
//         from: "mallalokesh23@gmail.com",
//         to: UserEmail,
//         subject: `Notification for New Add`,
//         text: `Hello, You have been invited to join . Click the following link to accept the invitation`,
//         html: `<h1>Hi '${emailTemplateContext.email}'</h1><p> Generated password is : '${emailTemplateContext.password}'</p>`,
//         html: `<!DOCTYPE html>
//                     <html lang="en">
                    
//                     <head>
//                         <meta charset="UTF-8">
//                         <meta http-equiv="X-UA-Compatible" content="IE=edge">
//                         <meta name="viewport" content="width=device-width, initial-scale=1.0">
//                         <title>Employee Invitation</title>
//                         <style>
//                     body{
//                         border: 1px solid green;
//                         padding: 20px;
//                         margin: 20px;
//                     }
//                     h2{
//                         color: #9A8B4F;
//                         font-family:  'Brush Script MT', cursive;
//                         font-size: 24px;
//                         font-weight: bold;
//                         text-align: center;
//                         margin-bottom: 20px;
//                     }
//                     .bg{
//                         background-image: url('https://i.pinimg.com/736x/36/57/c4/3657c4da1a560f03d4f47544db301913.jpg');
//                         background-size: 100% 100%; 
//                         width:500px;
//                         heigth:auto;
//                         padding: 20px 10px;
//                         border-radius: 10px;
//                         color: black;
//                         text-align: center;
//                         font-family: sans-serif;
//                         font-size: 16px;
//                         margin: 0 40px;
//                     }
//                     .title{
//                         margin-top:30px;
//                         padding-top: 5rem;
//                     }
//                         .logo {
//                 text-align: center;
//                 margin-bottom: 20px;
//               }
//               .logo img {
//                 max-width: 200px;
//                 height: auto;
//               }
//                     </style>
//                     </head>
//                     <body>
//                     <div class="bg">
//                         <div class="logo">
//                           <img src="../eva.jpg" alt="logo">
//                         </div>
//                         <h2 class="title">Invitation EMAIL</h2>
//                         <p>Hello, <strong>USER</strong></p>
//                         <p>Please find your login details below:</p>
//                         <p><strong>Email:</strong> '${emailTemplateContext.email}'</p>
//                         <p><strong>Password:</strong> '${emailTemplateContext.password}'</p>
//                         <p>Click the following link to accept the invitation:</p>
//                         <a href="{{invitationLink}}">Accept Invitation</a>
//                         <p>Thank you!</p></div>
//                     </body>
//                     </html>`,
//         context: emailTemplateContext,
//       };

//       await sendInvitationEmail(mailOptions);

//       return res.status(200).json({
//         success: true,
//         UserId: UserId,
//         message: "Role and Consultant updated successfully",
//       });
//     } else {
//       return res.status(404).json({
//         success: false,
//         message: "User not found",
//       });
//     }
//   } catch (error) {
//     return res.status(500).json({
//       success: false,
//       message: "Internal server error",
//       error: error.message,
//     });
//   }
// };


export const AddcompanyAdminByConsultantAdmin = async (req, res) => {
  try {
    const { UserEmail, ConsultantID ,TemporaryPassword} = req.body;
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (!emailRegex.test(UserEmail)) {
      return res.status(400).json({
        success: false,
        message: "Invalid email format",
      });
    }

    const pool = await getConnection();
    const userResult = await pool.request()
      .input("UserEmail", UserEmail)
      .query(`
        SELECT UserId FROM [dbo].[User] WHERE [UserEmail] = @UserEmail`);

    if (userResult.recordset.length > 0) {
      const UserId = userResult.recordset[0].UserId;

      await pool.request()
        .input("UserId", UserId)
        .query(`
          UPDATE [dbo].[Approval]
          SET [IsApproved] = '1'
          WHERE [UserId] = @UserId AND ([ApprovalType] = 'User Role Approval' OR [ApprovalType] = 'User Signup Approval')`);

      await pool.request()
        .input("UserId", UserId)
        .input("RoleId", 3)
        .input("ConsultantID", ConsultantID)
        .query(`
          UPDATE [dbo].[Approval]
          SET [RoleId] = @RoleId, [ConsultantID] = @ConsultantID
          WHERE [UserId] = @UserId AND [ApprovalType] = 'User Role Approval'`);

          const resetPasswordLink = await generatePasswordResetTicket(UserEmail);

          const emailTemplateContext = {
            email: UserEmail,
            password: TemporaryPassword,
            resetPasswordLink: resetPasswordLink
          };
      
          const mailOptions = {
            from: "Bhargav.Konathala@cloudomaxcorp.com",
            to: UserEmail,
            subject: `Welcome to EVA`,
            html: `<!DOCTYPE html>
                    <html lang="en">
                    <head>
                      <meta charset="UTF-8">
                      <meta http-equiv="X-UA-Compatible" content="IE=edge">
                      <meta name="viewport" content="width=device-width, initial-scale=1.0">
                      <title>Welcome to EVA</title>
                      <style>
                        body {
                          font-family: Arial, sans-serif;
                          background-color: #f4f4f4;
                          margin: 0;
                          padding: 0;
                          color: #333;
                        }
                        .container {
                          max-width: 600px;
                          margin: 50px auto;
                          background-color: #fff;
                          padding: 20px;
                          border-radius: 8px;
                          box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                        }
                        .header {
                          text-align: center;
                          background-color: #212121;
                          border-bottom: 1px solid #e6e6e6;
                          padding-bottom: 20px;
                          margin-bottom: 20px;
                        }
                        .header img {
                          margin-top: 10px;
                          max-width: 50px;
                          width: 20%;
                          display: inline-block;
                          vertical-align: middle;
                        }
                        .header h1 {
                          font-size: 24px;
                          color: #fff;
                          margin: 0;
                        }
                        .content {
                          text-align: center;
                        }
                        .content h2 {
                          font-size: 20px;
                          color: #333;
                          margin: 0 0 20px;
                        }
                        .content p {
                          font-size: 16px;
                          color: #666;
                          line-height: 1.6;
                        }
                        .btn-link {
                          background-color: #002984;
                          color: #ffffff !important;
                          text-decoration: none;
                          border-radius: 5px;
                          padding: 10px 20px;
                          display: inline-block;
                        }

                        .btn-link:hover {
                          background-color: #001c6c;
                          color: #ffffff !important;
                        }
                          .btn-reset {
                          color: #002984;
                          text-decoration: none;
                          font-weight: bold;
                        }
                        .footer {
                          text-align: center;
                          padding-top: 20px;
                          border-top: 1px solid #e6e6e6;
                          margin-top: 20px;
                          color: #999;
                          font-size: 14px;
                        }
                      </style>
                    </head>
                    <body>
                      <div class="container">
                        <div class="header">
                          <img src="https://coeindex0storage.blob.core.windows.net/file-uploads/EVA%20LOGO.png" alt="Logo" style="margin-top: 10px;">
                          <h1>Welcome to EVA</h1>
                        </div>
                        <div class="content">
                          <h2>Hello, ${emailTemplateContext.email}</h2>
                          <p>Welcome to EVA - Evaluation Virtual Assistance. We're excited to have you on board!</p>
                          <p>Please find your login details below:</p>
                          <p><strong>Email:</strong> ${emailTemplateContext.email}</p>
                          <p><strong>Temporary Password:</strong> ${emailTemplateContext.password}</p>
                          <p>For your security, please reset your password using the link below:</p>
                          <a href="${emailTemplateContext.resetPasswordLink}" class="btn-reset">Reset Password</a>
                          <p>Once your password is reset, you can log in to our website using the following link:</p>
                          <a href="https://app.evaomax.com/" class="btn-link">Log In to EVA</a>
                        </div>
                        <div class="footer">
                          <p>If you have any questions or need assistance, feel free to contact us at EVA@CloudOMax.com.</p>
                          <p>Thank you!</p>
                        </div>
                      </div>
                    </body>
                    </html>`,
            context: emailTemplateContext,
          };

      await sendInvitationEmail(mailOptions,emailTemplateContext);

      return res.status(200).json({
        success: true,
        UserId: UserId,
        message: "Role and Consultant updated & mail sent successfully",
      });
    } else {
      return res.status(404).json({
        success: false,
        message: "User not found",
      });
    }
  } catch (error) {
    return res.status(500).json({
      success: false,
      message: "Internal server error",
      error: error.message,
    });
  }
};
export const AddcompanyUserByConsultantAdmin = async (req, res) => {
  try {
    const { UserEmail, UserCompanyName, ConsultantID,TemporaryPassword } = req.body;
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (!emailRegex.test(UserEmail)) {
      return res.status(400).json({
        success: false,
        message: "Invalid email format",
      });
    }

    const pool = await getConnection();

    // Update the UserCompanyName
    const updateResult = await pool
      .request()
      .input("UserEmail", UserEmail)
      .input("UserCompanyName", UserCompanyName).query(`
        UPDATE [dbo].[User]
        SET [UserCompanyName] = @UserCompanyName
        WHERE [UserEmail] = @UserEmail
      `);

    // Fetch the updated user
    const userResult = await pool.request().input("UserEmail", UserEmail)
      .query(`
        SELECT UserId FROM [dbo].[User] WHERE [UserEmail] = @UserEmail
      `);

    if (userResult.recordset.length > 0) {
      const UserId = userResult.recordset[0].UserId;

      await pool.request().input("UserId", UserId).query(`
            UPDATE [dbo].[Approval] 
            SET [IsApproved] = '1'
            WHERE [UserId] = @UserId AND ([ApprovalType] = 'User Role Approval' OR [ApprovalType] = 'User Signup Approval')`);

      const approvalResult = await pool
        .request()
        .input("UserId", UserId)
        .input("ConsultantID", ConsultantID).query(`
          UPDATE [dbo].[Approval]
          SET [ConsultantID] = @ConsultantID
          WHERE [UserId] = @UserId AND [ApprovalType] = 'User Role Approval'
        `);

        const resetPasswordLink = await generatePasswordResetTicket(UserEmail);

          const emailTemplateContext = {
            email: UserEmail,
            password: TemporaryPassword,
            resetPasswordLink: resetPasswordLink
          };
      
          const mailOptions = {
            from: "Bhargav.Konathala@cloudomaxcorp.com",
            to: UserEmail,
            subject: `Welcome to EVA`,
            html: `<!DOCTYPE html>
                    <html lang="en">
                    <head>
                      <meta charset="UTF-8">
                      <meta http-equiv="X-UA-Compatible" content="IE=edge">
                      <meta name="viewport" content="width=device-width, initial-scale=1.0">
                      <title>Welcome to EVA</title>
                      <style>
                        body {
                          font-family: Arial, sans-serif;
                          background-color: #f4f4f4;
                          margin: 0;
                          padding: 0;
                          color: #333;
                        }
                        .container {
                          max-width: 600px;
                          margin: 50px auto;
                          background-color: #fff;
                          padding: 20px;
                          border-radius: 8px;
                          box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                        }
                        .header {
                          text-align: center;
                          background-color: #212121;
                          border-bottom: 1px solid #e6e6e6;
                          padding-bottom: 20px;
                          margin-bottom: 20px;
                        }
                        .header img {
                          margin-top: 10px;
                          max-width: 50px;
                          width: 20%;
                          display: inline-block;
                          vertical-align: middle;
                        }
                        .header h1 {
                          font-size: 24px;
                          color: #fff;
                          margin: 0;
                        }
                        .content {
                          text-align: center;
                        }
                        .content h2 {
                          font-size: 20px;
                          color: #333;
                          margin: 0 0 20px;
                        }
                        .content p {
                          font-size: 16px;
                          color: #666;
                          line-height: 1.6;
                        }
                        .btn-link {
                          background-color: #002984;
                          color: #ffffff !important;
                          text-decoration: none;
                          border-radius: 5px;
                          padding: 10px 20px;
                          display: inline-block;
                        }

                        .btn-link:hover {
                          background-color: #001c6c;
                          color: #ffffff !important;
                        }

                        .btn-reset {
                          color: #002984;
                          text-decoration: none;
                          font-weight: bold;
                        }

                        .footer {
                          text-align: center;
                          padding-top: 20px;
                          border-top: 1px solid #e6e6e6;
                          margin-top: 20px;
                          color: #999;
                          font-size: 14px;
                        }
                      </style>
                    </head>
                    <body>
                      <div class="container">
                        <div class="header">
                          <img src="https://coeindex0storage.blob.core.windows.net/file-uploads/EVA%20LOGO.png" alt="Logo" style="margin-top: 10px;">
                          <h1>Welcome to EVA</h1>
                        </div>
                        <div class="content">
                          <h2>Hello, ${emailTemplateContext.email}</h2>
                          <p>Welcome to EVA - Evaluation Virtual Assistance. We're excited to have you on board!</p>
                          <p>Please find your login details below:</p>
                          <p><strong>Email:</strong> ${emailTemplateContext.email}</p>
                          <p><strong>Temporary Password:</strong> ${emailTemplateContext.password}</p>
                          <p>For your security, please reset your password using the link below:</p>
                          <a href="${emailTemplateContext.resetPasswordLink}" class="btn-reset">Reset Password</a>
                          <p>Once your password is reset, you can log in to our website using the following link:</p>
                          <a href="https://app.evaomax.com/" class="btn-link">Log In to EVA</a>
                        </div>
                        <div class="footer">
                          <p>If you have any questions or need assistance, feel free to contact us at EVA@CloudOMax.com.</p>
                          <p>Thank you!</p>
                        </div>
                      </div>
                    </body>
                    </html>`,
            context: emailTemplateContext,
          };
        await sendInvitationEmail(mailOptions,emailTemplateContext);
  

      return res.status(200).json({
        success: true,
        UserId: UserId,
        message: "Company and Consultant updated  mail sent successfully",
      });
    } else {
      return res.status(404).json({
        success: false,
        message: "User not found",
      });
    }
  } catch (error) {
    return res.status(500).json({
      success: false,
      message: "Internal server error",
      error: error.message,
    });
  }
};

export const createNonCompanyUser = async (req, res) => {
  try {
    const { UserEmail,TemporaryPassword } = req.body;
    console.log(UserEmail,TemporaryPassword)
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

    if (!emailRegex.test(UserEmail)) {
      return res.status(400).json({
        success: false,
        message: "Invalid email format",
      });
    }

    const pool = await getConnection();
    const userResult = await pool.request().input("UserEmail", UserEmail)
      .query(`
          SELECT UserId FROM [dbo].[User] WHERE [UserEmail] = @UserEmail`);

    if (userResult.recordset.length > 0) {
      const UserId = userResult.recordset[0].UserId;

      const updateUserCompanyResult = await pool
        .request()
        .input("UserId", UserId).query(`
              UPDATE [dbo].[Approval]
              SET [IsApproved] = '1'
              WHERE [UserId] = @UserId AND ([ApprovalType] = 'User Role Approval' OR [ApprovalType] = 'User Signup Approval')
          `);
          const resetPasswordLink = await generatePasswordResetTicket(UserEmail);

          const emailTemplateContext = {
            email: UserEmail,
            password: TemporaryPassword,
            resetPasswordLink: resetPasswordLink
          };
      
          const mailOptions = {
            from: "Bhargav.Konathala@cloudomaxcorp.com",
            to: UserEmail,
            subject: `Welcome to EVA`,
            html: `<!DOCTYPE html>
                    <html lang="en">
                    <head>
                      <meta charset="UTF-8">
                      <meta http-equiv="X-UA-Compatible" content="IE=edge">
                      <meta name="viewport" content="width=device-width, initial-scale=1.0">
                      <title>Welcome to EVA</title>
                      <style>
                        body {
                          font-family: Arial, sans-serif;
                          background-color: #f4f4f4;
                          margin: 0;
                          padding: 0;
                          color: #333;
                        }
                        .container {
                          max-width: 600px;
                          margin: 50px auto;
                          background-color: #fff;
                          padding: 20px;
                          border-radius: 8px;
                          box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                        }
                        .header {
                          text-align: center;
                          background-color: #212121;
                          border-bottom: 1px solid #e6e6e6;
                          padding-bottom: 20px;
                          margin-bottom: 20px;
                        }
                        .header img {
                          margin-top: 10px;
                          max-width: 50px;
                          width: 20%;
                          display: inline-block;
                          vertical-align: middle;
                        }
                        .header h1 {
                          font-size: 24px;
                          color: #fff;
                          margin: 0;
                        }
                        .content {
                          text-align: center;
                        }
                        .content h2 {
                          font-size: 20px;
                          color: #333;
                          margin: 0 0 20px;
                        }
                        .content p {
                          font-size: 16px;
                          color: #666;
                          line-height: 1.6;
                        }
                        .btn-link {
                          background-color: #002984;
                          color: #ffffff !important;
                          text-decoration: none;
                          border-radius: 5px;
                          padding: 10px 20px;
                          display: inline-block;
                        }

                        .btn-link:hover {
                          background-color: #001c6c;
                          color: #ffffff !important;
                        }

                        .btn-reset {
                          color: #002984;
                          text-decoration: none;
                          font-weight: bold;
                        }
                        .footer {
                          text-align: center;
                          padding-top: 20px;
                          border-top: 1px solid #e6e6e6;
                          margin-top: 20px;
                          color: #999;
                          font-size: 14px;
                        }
                      </style>
                    </head>
                    <body>
                      <div class="container">
                        <div class="header">
                          <img src="https://coeindex0storage.blob.core.windows.net/file-uploads/EVA%20LOGO.png" alt="Logo" style="margin-top: 10px;">
                          <h1>Welcome to EVA</h1>
                        </div>
                        <div class="content">
                          <h2>Hello, ${emailTemplateContext.email}</h2>
                          <p>Welcome to EVA - Evaluation Virtual Assistance. We're excited to have you on board!</p>
                          <p>Please find your login details below:</p>
                          <p><strong>Email:</strong> ${emailTemplateContext.email}</p>
                          <p><strong>Temporary Password:</strong> ${emailTemplateContext.password}</p>
                          <p>For your security, please reset your password using the link below:</p>
                          <a href="${emailTemplateContext.resetPasswordLink}" class="btn-reset">Reset Password</a>
                          <p>Once your password is reset, you can log in to our website using the following link:</p>
                          <a href="https://app.evaomax.com/" class="btn-link">Log In to EVA</a>
                        </div>
                        <div class="footer">
                          <p>If you have any questions or need assistance, feel free to contact us at EVA@CloudOMax.com.</p>
                          <p>Thank you!</p>
                        </div>
                      </div>
                    </body>
                    </html>`,
            context: emailTemplateContext,
          };
    
          await sendInvitationEmail(mailOptions,emailTemplateContext);
    

      return res.status(200).json({
        success: true,
        UserId: UserId,
        message: "User created successfully, email sent with password reset link.",
      });
    } else {
      return res.status(404).json({
        success: false,
        message: "User not found",
      });
    }
  } catch (error) {
    console.error("Error updating  approvals:", error);
    return res.status(500).json({
      success: false,
      message: "Internal server error",
      error: error.message,
    });
  }
};


// export const updateUserApproval = async (req, res) => {
//   try {
//     const { UserID, RoleID, IsApproved } = req.body;

//     // Validate the input data
//     if (
//       typeof UserID !== "number" ||
//       (IsApproved !== undefined && typeof IsApproved !== "number")
//     ) {
//       return res.status(400).json({
//         success: false,
//         message: "Invalid input data",
//       });
//     }

//     const pool = await getConnection();

//     // Retrieve UserEmail and Name by UserID
//     const userResult = await pool
//       .request()
//       .input("UserID", UserID)
//       .query(`
//         SELECT 
//           [UserEmail],
//           [UserFirstName] + ' ' + [UserLastName] AS [name]
//         FROM [dbo].[Users]
//         WHERE [UserID] = @UserID;
//       `);

//     if (userResult.recordset.length === 0) {
//       return res.status(404).json({
//         success: false,
//         message: "User not found",
//       });
//     }

//     const UserEmail = userResult.recordset[0].UserEmail;
//     const UserName = userResult.recordset[0].name;

//     let updateIsApprovalResult;
//     let updateRoleIDResult;

//     // Update IsApproved if provided
//     if (IsApproved !== undefined) {
//       updateIsApprovalResult = await pool
//         .request()
//         .input("UserID", UserID)
//         .input("IsApproved", IsApproved)
//         .query(`
//           UPDATE [dbo].[Approval]
//           SET [IsApproved] = @IsApproved
//           WHERE [UserID] = @UserID;
//         `);

//       Logger.info("User approval status updated successfully", {
//         service: "user_details",
//       });

//       // Send email based on approval status
//       if (updateIsApprovalResult.rowsAffected[0] > 0) {
//         let mailOptions;

//         if (IsApproved === 1) {
//           mailOptions = {
//             from: "mallalokesh23@gmail.com",
//             to: UserEmail,
//             subject: "Your EVA Account Has Been Approved",
//             html: `<!DOCTYPE html>
//                     <html lang="en">
//                     <head>
//                       <meta charset="UTF-8">
//                       <meta http-equiv="X-UA-Compatible" content="IE=edge">
//                       <meta name="viewport" content="width=device-width, initial-scale=1.0">
//                       <title>Account Approved - EVA</title>
//                       <style>
//                         body {
//                           font-family: Arial, sans-serif;
//                           background-color: #f4f4f4;
//                           margin: 0;
//                           padding: 0;
//                           color: #333;
//                         }
//                         .container {
//                           max-width: 600px;
//                           margin: 50px auto;
//                           background-color: #fff;
//                           padding: 20px;
//                           border-radius: 8px;
//                           box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
//                         }
//                         .header {
//                           text-align: center;
//                           background-color: #212121;
//                           border-bottom: 1px solid #e6e6e6;
//                           padding-bottom: 20px;
//                           margin-bottom: 20px;
//                         }
//                         .header img {
//                           margin-top: 10px;
//                           max-width: 50px;
//                           width: 20%;
//                           display: inline-block;
//                           vertical-align: middle;
//                         }
//                         .header h1 {
//                           font-size: 24px;
//                           color: #fff;
//                           margin: 0;
//                         }
//                         .content {
//                           text-align: center;
//                         }
//                         .content h2 {
//                           font-size: 20px;
//                           color: #333;
//                           margin: 0 0 20px;
//                         }
//                         .content p {
//                           font-size: 16px;
//                           color: #666;
//                           line-height: 1.6;
//                         }
//                         .btn-link {
//                           background-color: #002984;
//                           color: #ffffff !important;
//                           text-decoration: none;
//                           border-radius: 5px;
//                           padding: 10px 20px;
//                           display: inline-block;
//                         }

//                         .btn-link:hover {
//                           background-color: #001c6c;
//                           color: #ffffff !important;
//                         }
//                         .footer {
//                           text-align: center;
//                           padding-top: 20px;
//                           border-top: 1px solid #e6e6e6;
//                           margin-top: 20px;
//                           color: #999;
//                           font-size: 14px;
//                         }
//                       </style>
//                     </head>
//                     <body>
//                       <div class="container">
//                         <div class="header">
//                           <img src="https://coeindex0storage.blob.core.windows.net/file-uploads/EVA%20LOGO.png" alt="Logo" style="margin-top: 10px;">
//                           <h1>Account Approved - EVA</h1>
//                         </div>
//                         <div class="content">
//                           <h2>Hello, ${UserName}</h2>
//                           <p>We are pleased to inform you that your account with EVA has been approved.</p>
//                           <p>You can now log in with your login credentials.</p>
//                           <p>Please use the link below to log in:</p>
//                           <a href="https://app.evaomax.com/" class="btn-link">Log In</a>
//                         </div>
//                         <div class="footer">
//                           <p>If you have any questions or need assistance, feel free to contact us at EVA@CloudOMax.com.</p>
//                           <p>Thank you!</p>
//                         </div>
//                       </div>
//                     </body>
//                     </html>`,
//           };
//         } else if (IsApproved === 0) {
//           mailOptions = {
//             from: "mallalokesh23@gmail.com",
//             to: UserEmail,
//             subject: "Your EVA Account is Now On Hold",
//             html: `<!DOCTYPE html>
//                     <html lang="en">
//                     <head>
//                       <meta charset="UTF-8">
//                       <meta http-equiv="X-UA-Compatible" content="IE=edge">
//                       <meta name="viewport" content="width=device-width, initial-scale=1.0">
//                       <title>Account On Hold - EVA</title>
//                       <style>
//                         body {
//                           font-family: Arial, sans-serif;
//                           background-color: #f4f4f4;
//                           margin: 0;
//                           padding: 0;
//                           color: #333;
//                         }
//                         .container {
//                           max-width: 600px;
//                           margin: 50px auto;
//                           background-color: #fff;
//                           padding: 20px;
//                           border-radius: 8px;
//                           box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
//                         }
//                         .header {
//                           text-align: center;
//                           background-color: #212121;
//                           border-bottom: 1px solid #e6e6e6;
//                           padding-bottom: 20px;
//                           margin-bottom: 20px;
//                         }
//                         .header img {
//                           margin-top: 10px;
//                           max-width: 50px;
//                           width: 20%;
//                           display: inline-block;
//                           vertical-align: middle;
//                         }
//                         .header h1 {
//                           font-size: 24px;
//                           color: #fff;
//                           margin: 0;
//                         }
//                         .content {
//                           text-align: center;
//                         }
//                         .content h2 {
//                           font-size: 20px;
//                           color: #333;
//                           margin: 0 0 20px;
//                         }
//                         .content p {
//                           font-size: 16px;
//                           color: #666;
//                           line-height: 1.6;
//                         }
//                         .btn-link {
//                           background-color: #002984;
//                           color: #ffffff !important;
//                           text-decoration: none;
//                           border-radius: 5px;
//                           padding: 10px 20px;
//                           display: inline-block;
//                         }

//                         .btn-link:hover {
//                           background-color: #001c6c;
//                           color: #ffffff !important;
//                         }
//                         .footer {
//                           text-align: center;
//                           padding-top: 20px;
//                           border-top: 1px solid #e6e6e6;
//                           margin-top: 20px;
//                           color: #999;
//                           font-size: 14px;
//                         }
//                       </style>
//                     </head>
//                     <body>
//                       <div class="container">
//                         <div class="header">
//                           <img src="https://coeindex0storage.blob.core.windows.net/file-uploads/EVA%20LOGO.png" alt="Logo" style="margin-top: 10px;">
//                           <h1>Account On Hold - EVA</h1>
//                         </div>
//                         <div class="content">
//                           <h2>Hello, ${UserName}</h2>
//                           <p>We regret to inform you that your EVA account status has been changed to 'On Hold'.</p>
//                           <p>Please wait for further instructions from our support team.</p>
//                           <p>If you have any questions or need assistance, feel free to contact us at EVA@CloudOMax.com.</p>
//                         </div>
//                         <div class="footer">
//                           <p>Thank you!</p>
//                         </div>
//                       </div>
//                     </body>
//                     </html>`,
//           };
//         }

//         mailer.sendMail(mailOptions, (error, info) => {
//           if (error) {
//             Logger.error("Error sending approval email", { error });
//           } else {
//             Logger.info("Approval email sent: " + info.response);
//           }
//         });
//       }
//     }

//     // Update RoleID if provided
//     if (RoleID !== undefined) {
//       updateRoleIDResult = await pool
//         .request()
//         .input("UserID", UserID)
//         .input("RoleID", RoleID)
//         .query(`
//           UPDATE [dbo].[Approval]
//           SET [RoleID] = @RoleID
//           WHERE [UserID] = @UserID AND [ApprovalType] = 'User Role Approval';
//         `);

//       Logger.info("Role ID updated successfully", { service: "user_details" });

//       // Send role update email
//       if (updateRoleIDResult.rowsAffected[0] > 0) {
//         const newRoleResult = await pool
//           .request()
//           .input("UserID", UserID)
//           .query(`
//             SELECT [RoleID]
//             FROM [dbo].[Approval]
//             WHERE [UserID] = @UserID;
//           `);

//         const newRole = newRoleResult.recordset[0].RoleID;

//         const mailOptionsRoleUpdate = {
//           from: "mallalokesh23@gmail.com",
//           to: UserEmail,
//           subject: "Your Role in EVA Has Been Updated",
//           html: `<!DOCTYPE html>
//                   <html lang="en">
//                   <head>
//                     <meta charset="UTF-8">
//                     <meta http-equiv="X-UA-Compatible" content="IE=edge">
//                     <meta name="viewport" content="width=device-width, initial-scale=1.0">
//                     <title>Role Update - EVA</title>
//                     <style>
//                       body {
//                         font-family: Arial, sans-serif;
//                         background-color: #f4f4f4;
//                         margin: 0;
//                         padding: 0;
//                         color: #333;
//                       }
//                       .container {
//                         max-width: 600px;
//                         margin: 50px auto;
//                         background-color: #fff;
//                         padding: 20px;
//                         border-radius: 8px;
//                         box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
//                       }
//                       .header {
//                         text-align: center;
//                         background-color: #212121;
//                         border-bottom: 1px solid #e6e6e6;
//                         padding-bottom: 20px;
//                         margin-bottom: 20px;
//                       }
//                       .header img {
//                         margin-top: 10px;
//                         max-width: 50px;
//                         width: 20%;
//                         display: inline-block;
//                         vertical-align: middle;
//                       }
//                       .header h1 {
//                         font-size: 24px;
//                         color: #fff;
//                         margin: 0;
//                       }
//                       .content {
//                         text-align: center;
//                       }
//                       .content h2 {
//                         font-size: 20px;
//                         color: #333;
//                         margin: 0 0 20px;
//                       }
//                       .content p {
//                         font-size: 16px;
//                         color: #666;
//                         line-height: 1.6;
//                       }
//                       .btn-link {
//                         background-color: #002984;
//                         color: #ffffff !important;
//                         text-decoration: none;
//                         border-radius: 5px;
//                         padding: 10px 20px;
//                         display: inline-block;
//                       }

//                       .btn-link:hover {
//                         background-color: #001c6c;
//                         color: #ffffff !important;
//                       }
//                       .footer {
//                         text-align: center;
//                         padding-top: 20px;
//                         border-top: 1px solid #e6e6e6;
//                         margin-top: 20px;
//                         color: #999;
//                         font-size: 14px;
//                       }
//                     </style>
//                   </head>
//                   <body>
//                     <div class="container">
//                       <div class="header">
//                         <img src="https://coeindex0storage.blob.core.windows.net/file-uploads/EVA%20LOGO.png" alt="Logo" style="margin-top: 10px;">
//                         <h1>Role Update - EVA</h1>
//                       </div>
//                       <div class="content">
//                         <h2>Hello, ${UserName}</h2>
//                         <p>We are pleased to inform you that your role within EVA has been updated.</p>
//                         <p>Your new role is: <strong>${newRole}</strong></p>
//                         <p>Please use the link below to log in:</p>
//                         <a href="https://app.evaomax.com/" class="btn-link">Log In</a>
//                       </div>
//                       <div class="footer">
//                         <p>If you have any questions or need assistance, feel free to contact us at EVA@CloudOMax.com.</p>
//                         <p>Thank you!</p>
//                       </div>
//                     </div>
//                   </body>
//                   </html>`,
//         };

//         mailer.sendMail(mailOptionsRoleUpdate, (error, info) => {
//           if (error) {
//             Logger.error("Error sending role update email", { error });
//           } else {
//             Logger.info("Role update email sent: " + info.response);
//           }
//         });
//       }
//     }

//     // Check if any updates were made
//     if (
//       (updateIsApprovalResult && updateIsApprovalResult.rowsAffected[0] === 0) &&
//       (updateRoleIDResult && updateRoleIDResult.rowsAffected[0] === 0)
//     ) {
//       return res.status(404).json({
//         success: false,
//         message: "User not found or no changes made",
//       });
//     }

//     res.status(200).json({
//       success: true,
//       message: "User approval updated successfully",
//     });
//   } catch (error) {
//     Logger.error("Error updating user approval", {
//       service: "user_details",
//       error,
//     });
//     res.status(500).json({
//       success: false,
//       message: "Internal server error",
//     });
//   }
// };


