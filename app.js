import express from 'express';
import bodyParser from 'body-parser';
import sql from 'mssql';
import cors from 'cors';
import dotenv from 'dotenv';
import path from 'path';
import multer from 'multer';

// code!!!
// Load environment variables
dotenv.config();

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const __dirname = path.resolve();
app.use(cors());

const dbConfig = {
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  server: process.env.DB_SERVER,
  database: process.env.DB_NAME,
  options: {
    encrypt: true,
    trustServerCertificate: true
  }
};

async function connectToDatabase() {
  try { 
    await sql.connect(dbConfig);
    console.log('Connected to SQL Server');

  } catch (err) {
    console.error('Connection or query error', err);
  } finally {
    // It's better to close the connection when you're actually done with all queries
    // await sql.close();
  }
}
connectToDatabase();

// Routes init
app.get('/', (req, res) => {
  res.send("Hello sql ms");
});
import users from "./routers/user.router.js"
app.use("/users",users)

const port = process.env.PORT || 5200;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
