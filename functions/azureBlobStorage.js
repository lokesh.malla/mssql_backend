import { BlobServiceClient, StorageSharedKeyCredential } from '@azure/storage-blob';
import 'dotenv/config';

const accountName = process.env.AZURE_STORAGE_ACCOUNT_NAME;
const accountKey = process.env.AZURE_STORAGE_ACCOUNT_KEY;

if (!accountName || !accountKey) {
  throw new Error('Azure Storage account name or account key is missing in .env file');
}

const sharedKeyCredential = new StorageSharedKeyCredential(accountName, accountKey);
const blobServiceClient = new BlobServiceClient(`https://${accountName}.blob.core.windows.net`, sharedKeyCredential);

export const uploadFileToBlob = async (containerName, blobName, fileBuffer, contentType) => {
  const containerClient = blobServiceClient.getContainerClient(containerName);
  const blockBlobClient = containerClient.getBlockBlobClient(blobName);
  
  await blockBlobClient.uploadData(fileBuffer, {
    blobHTTPHeaders: { blobContentType: contentType },
  });

  return blockBlobClient.url;
};

export const getBlobUrl = async (containerName, blobPrefix) => {
  const containerClient = blobServiceClient.getContainerClient(containerName);
  const blobIterator = containerClient.listBlobsFlat({ prefix: blobPrefix });

  for await (const blob of blobIterator) {
    if (blob.name.startsWith(blobPrefix)) {
      return `${blobServiceClient.url}${containerName}/${blob.name}`;
    }
  }

  return null;
};

export const deleteFileFromBlob = async (containerName, blobPrefix, surveyName) => {
  const containerClient = blobServiceClient.getContainerClient(containerName);
  const blobIterator = containerClient.listBlobsFlat({ prefix: blobPrefix });

  for await (const blob of blobIterator) {
    const fileName = blob.name.split('/').pop();
    const fileNameWithoutExtension = fileName.replace('.pdf', '');
    const fileNameParts = fileNameWithoutExtension.split(' - ');
    const extractedSurveyName = fileNameParts.slice(1).join(' - ').trim().toLowerCase();
    if (extractedSurveyName === surveyName.toLowerCase()) {
      const blockBlobClient = containerClient.getBlockBlobClient(blob.name);
      await blockBlobClient.deleteIfExists();
      console.log(`Deleted blob: ${blob.name}`);
    }
  }

  return null;
};