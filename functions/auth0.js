import axios from 'axios';
import dotenv from 'dotenv';

dotenv.config();

const getAuth0ManagementToken = async () => {
  const options = {
    method: 'POST',
    url: `${process.env.AUTH0_DOMAIN}/oauth/token`,
    headers: { 'content-type': 'application/json' },
    data: {
      client_id: process.env.AUTH0_CLIENT_ID,
      client_secret: process.env.AUTH0_CLIENT_SECRET,
      audience: `${process.env.AUTH0_DOMAIN}/api/v2/`,
      grant_type: 'client_credentials'
    }
  };

  try {
    const response = await axios.request(options);
    return response.data.access_token;
  } catch (error) {
    console.error('Error getting Auth0 Management token:', error);
    throw error;
  }
};

  
  const generatePasswordResetTicket = async (userEmail) => {
    const token = await getAuth0ManagementToken();
  
    const options = {
      method: 'POST',
      url: `${process.env.AUTH0_DOMAIN}/api/v2/tickets/password-change`,
      headers: {
        'content-type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      data: {
        email: userEmail,
        connection_id: process.env.AUTH0_CONNECTION_ID,
        ttl_sec: 86400 
      }
    };
  
    try {
      const response = await axios.request(options);
      return response.data.ticket;
    } catch (error) {
      console.error('Error generating password reset ticket:', error);
      throw error;
    }
  };

  const generateRandomPassword = (length) => {
    const charset = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+';
    let password = '';
    for (let i = 0; i < length; i++) {
      const randomIndex = Math.floor(Math.random() * charset.length);
      password += charset[randomIndex];
    }
    return password;
  };
  
  const getAccessToken = async () => {
    try {
      const auth0Domain = `${process.env.AUTH0_DOMAIN}/oauth/token`;
      const response = await axios.post(auth0Domain, {
        client_id: process.env.AUTH0_CLIENT_ID,
        client_secret: process.env.AUTH0_CLIENT_SECRET,
        audience: `${process.env.AUTH0_DOMAIN}/api/v2/`,
        grant_type: 'client_credentials',
      }, {
        headers: {
          'Content-Type': 'application/json',
        },
      });
  
      return response.data.access_token;
    } catch (error) {
      console.error('Error fetching token:', error);
      throw error;
    }
  };
  
  export { getAuth0ManagementToken, generatePasswordResetTicket , generateRandomPassword, getAccessToken};