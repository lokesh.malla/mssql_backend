import nodemailer from 'nodemailer';
import dotenv from 'dotenv';
dotenv.config();

const sendInvitationEmail = async ({ from, to, subject, text, html, context }) => {
    const transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.EMAIL_USER,
            pass: process.env.EMAIL_PASS
        }
    });

    const mailOptions = {
        from,
        to,
        subject,
        text,
        html: html.replace(/{{email}}/g, context.email)
                   .replace(/{{password}}/g, context.password)
                   .replace(/{{invitationLink}}/g, context.invitationLink)
    };

    try {
        const info = await transporter.sendMail(mailOptions);
        console.log('Email sent:', info.response);
    } catch (error) {
        console.error('Error sending invitation email:', error);
        throw new Error('Error sending invitation email: ' + error.message);
    }
};

export default sendInvitationEmail;
