import express from "express";
import multer from 'multer';

import {getSurveyStatus,updateUserById,deleteAssignedSurveyFromUser,getAllSurveyByUserId,getAllSurveyByCompanyId, getAllUsers, getCompanies, getRoles, getUserApproval, insertNewRecord, insertRecordByUserid, insertRecordsByCompany, registerUser, updateUserApproval, GetUserByID, uploadPdf, getPdfsByUserId, deleteTrackerId, createConsultantAdmin, createCompanyAdmin, CreateCompanyUser, createCompany, AddcompanyAdminByConsultantAdmin, AddcompanyUserByConsultantAdmin, createNonCompanyUser,getIndividualUsers,getUsersWithCompany,getUsersWithRoleID,getUnapprovedUserSignups,getApprovedCompanyAdminsBySuperAdmin,getApprovedConsultantAdminsBySuperAdmin,getCompanyAdminsByConsultant,getCompanyUsersByConsultant,getCompaniesByConsultant,getCompanyUsersByCompanyAdmin ,buildSurveyRequest,getAllBuildSurveys,deleteBuildSurvey,createUser, deleteUserFromAuth0,updateSurveyStatus,uploadProfileImage,getProfileImageUrl} from "../controllers/user.js";
const router = express.Router();
const storage = multer.memoryStorage();
const upload = multer({ storage: storage });


router.get("/getAllUsers",getAllUsers);
router.post("/registerUser",registerUser);
router.get("/getRoles",getRoles);
router.get("/getUserApproval",getUserApproval);
router.get("/getIndividualUsers",getIndividualUsers);
router.get("/getUsersWithCompany",getUsersWithCompany);
router.get("/getUnapprovedUserSignups",getUnapprovedUserSignups);
router.get("/getApprovedCompanyAdminsBySuperAdmin",getApprovedCompanyAdminsBySuperAdmin);
router.get("/getApprovedConsultantAdminsBySuperAdmin",getApprovedConsultantAdminsBySuperAdmin);
router.get("/getCompanyAdminsByConsultant",getCompanyAdminsByConsultant);
router.get("/getCompanyUsersByConsultant",getCompanyUsersByConsultant);
router.get("/getCompanyUsersByCompanyAdmin",getCompanyUsersByCompanyAdmin);
router.get("/getCompaniesByConsultant",getCompaniesByConsultant);
router.get("/getUsersWithRoleID",getUsersWithRoleID);
router.post("/updateUserApproval",updateUserApproval);
router.post("/insertNewRecord",insertNewRecord);
router.get("/getCompanies",getCompanies);
router.post("/insertRecordsByCompany",insertRecordsByCompany);
router.post("/insertRecordsByUserid",insertRecordByUserid);
router.get("/getAllSurveyByCompanyId",getAllSurveyByCompanyId);
router.get("/getSurveyStatus",getSurveyStatus);
router.get("/getAllSurveyByUserId",getAllSurveyByUserId);
router.post("/deleteAssignedSurveyFromUser",deleteAssignedSurveyFromUser);
router.get("/GetUserByID",GetUserByID);
router.post("/updateUserById",updateUserById);	
// router.post("/uploadPdf",uploadPdf)
router.post('/uploadPdf', upload.array('files'), uploadPdf);
router.post('/uploadProfileImage', upload.single('file'), uploadProfileImage);
router.get('/getProfileImageUrl',getProfileImageUrl);
router.get('/getPdfsByUserId',getPdfsByUserId);
router.post("/deleteTrackerId",deleteTrackerId);
// router.post("/deleteAllTrackerId",deleteAllTrackerId);
router.post("/createConsultantAdmin",createConsultantAdmin);
router.post("/createCompanyAdmin",createCompanyAdmin);
router.post("/CreateCompanyUser",CreateCompanyUser)
router.post("/createCompany",createCompany)
router.post("/AddcompanyAdminByConsultantAdmin",AddcompanyAdminByConsultantAdmin)
router.post("/AddcompanyUserByConsultantAdmin",AddcompanyUserByConsultantAdmin);
router.post("/createNonCompanyUser",createNonCompanyUser)
router.post("/buildSurveyRequest",buildSurveyRequest)
router.get("/getAllBuildSurveys",getAllBuildSurveys)
router.post("/deleteBuildSurvey",deleteBuildSurvey)
router.post("/createUser",createUser)
router.delete("/deleteUserfromAuth0",deleteUserFromAuth0)
router.post("/updateSurveyStatus",updateSurveyStatus)
// router.delete("/deleteUser",deleteUser)



export default router;